#include "CollisionDetection.h"
using std::min; using std::max;

double dot(Vec3d a,Vec3d b){ return a.dot(b); }
Vec3d cross(Vec3d a,Vec3d b){ return a.cross(b); }

void constructSpatialHashTable(int numPoints, float* points, multi_map &x, float gridSize)
{
	int hash_size = numPoints + 13;
	ihash hash_fct(hash_size);
	x = multi_map(hash_size,hash_fct);
	
	for(int i = 0; i < numPoints; i++)
	{
		Vec3d v(points[3*i],points[3*i+1],points[3*i+2]);
		x.insert(multi_map::value_type(Cell(v,gridSize),v)); 
	}
}


void solidCollisionTest(int numPoints1, float* points1, int numPoints2, float* points2, int numElems, int* elem2, float gridSize)
{
	multi_map x;
	constructSpatialHashTable(numPoints1,points1,x,gridSize);
	for(int i = 0; i < numElems; i++)
	{
		Vec3d lower, upper;
		Vec3d v0(points2[3*elem2[4*i]],points2[3*elem2[4*i]+1],points2[3*elem2[4*i]+2]);
		Vec3d v1(points2[3*elem2[4*i+1]],points2[3*elem2[4*i+1]+1],points2[3*elem2[4*i+1]+2]);
		Vec3d v2(points2[3*elem2[4*i+2]],points2[3*elem2[4*i+2]+1],points2[3*elem2[4*i+2]+2]);
		Vec3d v3(points2[3*elem2[4*i+3]],points2[3*elem2[4*i+3]+1],points2[3*elem2[4*i+3]+2]);

		boundingBox<Vec3d>(v0,v1,v2,v3,lower,upper);
		
		Cell L(lower,gridSize), U(upper,gridSize);
		for(int i1 = L.i; i1 <= U.i; i1++)
			for(int j1 = L.j; j1 <= U.j; j1++)
				for(int k1 = L.k; k1 <= U.k; k1++)
				{
					std::pair< multi_map::const_iterator, multi_map::const_iterator >
					r = x.equal_range(Cell(i1,j1,k1));
					
					for(multi_map::const_iterator it = r.first; it != r.second; it++)
					{
						if (testPointTetrahedral<Vec3d>(v0,v1,v2,v3,it->second)) 
							reportSolidCollision(v0,v1,v2,v3,it);
					}
				}
		
	}
}
void reportSolidCollision(Vec3d v0,Vec3d v1,Vec3d v2, Vec3d v3, multi_map::const_iterator it)
{
	printf("Point %f %f %f ",it->second[0],it->second[1],it->second[2]);
	printf("in Cell[%d %d %d]",it->first.i,it->first.j,it->first.k);
	printf("intersects element:");
	printf("\n");
}
void computeBoundingBox(int numPoints, float* points, Vec3d &lower, Vec3d &upper)
{
	float minx,miny,minz,maxx,maxy,maxz;
	minx = maxx = points[0];
	miny = maxy = points[1];
	minz = maxz = points[2];
	for(int i = 1; i < numPoints; i++)
	{
		if (points[3*i] < minx ) minx = points[3*i];
		if (points[3*i] > maxx ) maxx = points[3*i];
		if (points[3*i+1] < miny ) miny = points[3*i+1];
		if (points[3*i+1] > maxy ) maxy = points[3*i+1];
		if (points[3*i+2] < minz ) minz = points[3*i+2];
		if (points[3*i+2] > maxz ) maxz = points[3*i+2];
	}
	lower = Vec3d(minx,miny,minz);
	upper = Vec3d(maxx,maxy,maxz);
}

void visitCellTriangleOverlapped3d(float x1, float y1, float z1, float x2, float y2, float z2, float gridSize, 
	multi_map_tri &x, Triangle *tri, bool* mark, int nx, int ny, int nz)
{
	int i = (int)(x1/gridSize);
	int j = (int)(y1/gridSize);
	int k = (int)(z1/gridSize);

	int iend = (int)(x2/gridSize);
	int jend = (int)(y2/gridSize);
	int kend = (int)(z2/gridSize);

	int di = (x1 < x2)?1:((x1 > x2)?-1:0);
	int dj = (y1 < y2)?1:((y1 > y2)?-1:0);
	int dk = (z1 < z2)?1:((z1 > z2)?-1:0);

	float minx = gridSize*floor(x1/gridSize), maxx = minx + gridSize;
	float tx = ((x1<x2)?(x1-minx):(maxx-x1))/fabs(x2-x1);
	float miny = gridSize*floor(y1/gridSize), maxy = miny + gridSize;
	float ty = ((y1<y2)?(y1-miny):(maxy-y1))/fabs(y2-y1);
	float minz = gridSize*floor(z1/gridSize), maxz = minz + gridSize;
	float tz = ((z1<z2)?(z1-minz):(maxz-z1))/fabs(z2-z1);

	float delta_tx = gridSize/fabs(x2-x1);
	float delta_ty = gridSize/fabs(y2-y1);
	float delta_tz = gridSize/fabs(z2-z1);

	while(1)
	{
		if (!mark[k*nx*ny + j*nx + i]){
			x.insert(multi_map_tri::value_type(Cell(i,j,k),tri));
			mark[k*nx*ny + j*nx + i] = true;
		}
		if(tx <= ty && tx <= tz)
		{
			if (i == iend) break;
			tx += delta_tx;
			i += di;
		}
		else if (ty <= tx && ty <= tz)
		{
			if(j == jend) break;
			ty += delta_ty;
			j += dj;
		}
		else
		{
			if(k == kend) break;
			tz += delta_tz;
			k += dk;
		}
	}
}
void constructSpatialHashTable_Triangle(int numPoints, float* points, int numElems, int* elems, multi_map_tri &x, float gridSize)
{

	int hash_size = numPoints + 13;
	ihash hash_fct(hash_size);
	x = multi_map_tri(hash_size,hash_fct);

	for(int i = 0; i<numElems; i++)
	{
		float x0 = points[3*elems[3*i]]; float x1 = points[3*elems[3*i+1]]; float x2 = points[3*elems[3*i+2]];
		float y0 = points[3*elems[3*i]+1]; float y1 = points[3*elems[3*i+1]+1]; float y2 = points[3*elems[3*i+2]+1];
		float z0 = points[3*elems[3*i]+2]; float z1 = points[3*elems[3*i+1]+2]; float z2 = points[3*elems[3*i+2]+2];
		float min_x = min(min(x0,x1),x2); float min_y = min(min(y0,y1),y2); float min_z = min(min(z0,z1),z2);
		float max_x = max(max(x0,x1),x2); float max_y = max(max(y0,y1),y2); float max_z = max(max(z0,z1),z2);
		int i1 = int(min_x/gridSize); int j1 = int(min_y/gridSize); int k1 = int(min_z/gridSize);
		int i2 = int(max_x/gridSize); int j2 = int(max_y/gridSize); int k2 = int(max_z/gridSize);
		int nx = i2 - i1 + 1;
		int ny = j2 - j1 + 1;
		int nz = k2 - k1 + 1;
		int box_size = nx*ny*nz;
		bool* mark = (bool*)malloc(box_size*sizeof(bool));
		memset(mark,0,box_size*sizeof(bool));
		
		Triangle *tri = new Triangle(elems[3*i], elems[3*i+1],elems[3*i+2]);
		
		visitCellTriangleOverlapped3d(x1, y1, z1, x2, y2, z2, gridSize, 
	                          x, tri, mark, nx, ny, nz);
		visitCellTriangleOverlapped3d(x0, y0, z0, x2, y2, z2, gridSize, 
	                          x, tri, mark, nx, ny, nz);
		visitCellTriangleOverlapped3d(x1, y1, z1, x0, y0, z0, gridSize, 
	                          x, tri, mark, nx, ny, nz);
		free(mark);
	}
}
void rayTrianglesCollisionTest(Vec3d const &origin, Vec3d const &direction, int numPoints, float* points,int numElems, int* elems, float gridSize,
							Cell const &loweri, Cell const &upperi, multi_map_tri x, float &dist, Vec3d &n)
{
	int i = (int)(origin[0]/gridSize);
	int j = (int)(origin[1]/gridSize);
	int k = (int)(origin[2]/gridSize);

	int iend = (direction[0] >= 0)?(upperi.i+1):loweri.i;
	int jend = (direction[1] >= 0)?(upperi.j+1):loweri.j;
	int kend = (direction[2] >= 0)?(upperi.k+1):loweri.k;

	int di = (direction[0] > 0)?1:((direction[0] < 0)?-1:0);
	int dj = (direction[1] > 0)?1:((direction[1] < 0)?-1:0);
	int dk = (direction[2] > 0)?1:((direction[2] < 0)?-1:0);

	float minx = gridSize*floor(origin[0]/gridSize), maxx = minx + gridSize;
	float tx = ((direction[0] > 0)?(origin[0]-minx):(maxx-origin[0]))/fabs(direction[0]);
	float miny = gridSize*floor(origin[1]/gridSize), maxy = miny + gridSize;
	float ty = ((direction[1] > 0)?(origin[1]-miny):(maxy-origin[1]))/fabs(direction[1]);;
	float minz = gridSize*floor(origin[2]/gridSize), maxz = minz + gridSize;
	float tz = ((direction[0] > 0)?(origin[0]-minx):(maxx-origin[0]))/fabs(direction[0]);;

	float delta_tx = gridSize/fabs(direction[0]);
	float delta_ty = gridSize/fabs(direction[1]);
	float delta_tz = gridSize/fabs(direction[2]);

	float t,v,w;
	
	while(1)
	{
		// Processing Cell(i,j,k) 
		printf("Processing Cell(%d,%d,%d)...", i,j,k);
		std::pair<multi_map_tri::iterator, multi_map_tri::iterator> r = x.equal_range(Cell(i,j,k));
		for(multi_map_tri::iterator it = r.first; it != r.second; it++)
			if (it->second->time_stamp == 0)
			{
				Vec3d v0(points[3*(it->second->index[0])],points[3*(it->second->index[0])+1],points[3*(it->second->index[0])+2] );
				Vec3d v1(points[3*(it->second->index[1])],points[3*(it->second->index[1])+1],points[3*(it->second->index[1])+2] );
				Vec3d v2(points[3*(it->second->index[2])],points[3*(it->second->index[2])+1],points[3*(it->second->index[2])+2] );

				if(testRayTriangle<Vec3d>(origin, direction, v0, v1, v2, t, n))
				{
					if (t < dist) dist = t;
					printf("Current dist from origin %f\n", dist);
				}
				
				it->second->time_stamp = 1;
			}// Finish processing
		
		if(tx <= ty && tx <= tz)
		{
			if (i == iend) break;
			tx += delta_tx;
			i += di;
		}
		else if (ty <= tx && ty <= tz)
		{
			if(j == jend) break;
			ty += delta_ty;
			j += dj;
		}
		else
		{
			if(k == kend) break;
			tz += delta_tz;
			k += dk;
		}
	}
}
void rayTracingCollisionDetection(int numVertices1, float* vertices1, float* normals1, int numVertices2, float* vertices2, int numElems2, int* elems2)
{
	Vec3d minObj2, maxObj2;
	computeBoundingBox(numVertices2, vertices2, minObj2, maxObj2);
	
	float gridSize = .3;
	Cell loweri(minObj2, gridSize); Cell upperi(maxObj2, gridSize);
	multi_map_tri x;
	constructSpatialHashTable_Triangle(numVertices2, vertices2, numElems2, elems2, x, gridSize);
	
	for(int i = 0; i < numVertices1; i++)
	{
		Vec3d p1(vertices1[3*i],vertices1[3*i+1],vertices1[3*i+2]);
		Vec3d n1(-normals1[3*i],-normals1[3*i+1],-normals1[3*i+2]);
		Vec3d n2;
		float dist1 = 1e6;
		rayTrianglesCollisionTest(p1, n1, numVertices2, vertices2, numElems2, elems2, gridSize, loweri, upperi, x, dist1, n2);
		if (dot(n1,n2) <=0) continue;
		Vec3d p2 = p1 + dist1*n1;
		n1 *= (-1.0);
		float dist2 = 1e6;
		rayTrianglesCollisionTest(p2, n1, numVertices2, vertices2, numElems2, elems2, gridSize, loweri, upperi, x, dist2, n2);
		
		if (dist1 <= dist2) {
			PX$(p1);
			PX$(p2);
		}
	}

}
bool segmentCollisionDetection(float* startPt, float* endPt, int numVertices, float* vertices, int numElems, int* elems,
								  int& idx1, int& idx2, int& idx3, float& v, float& w)
{
	float gridSize = 1.3;
	
	multi_map_tri x;
	constructSpatialHashTable_Triangle(numVertices, vertices, numElems, elems, x, gridSize);
	
	int i = (int)(startPt[0]/gridSize);
	int j = (int)(startPt[1]/gridSize);
	int k = (int)(startPt[2]/gridSize);

	int iend = (int)(endPt[0]/gridSize);
	int jend = (int)(endPt[0]/gridSize);
	int kend = (int)(endPt[0]/gridSize);

	int di = (startPt[0] < endPt[0])?1:((startPt[0] > endPt[0])?-1:0);
	int dj = (startPt[1] < endPt[1])?1:((startPt[1] > endPt[1])?-1:0);
	int dk = (startPt[2] < endPt[2])?1:((startPt[2] > endPt[2])?-1:0);

	float minx = gridSize*floor(startPt[0]/gridSize), maxx = minx + gridSize;
	float tx = ((startPt[0]<endPt[0])?(startPt[0]-minx):(maxx-startPt[0]))/fabs(endPt[0]-startPt[0]);
	float miny = gridSize*floor(startPt[1]/gridSize), maxy = miny + gridSize;
	float ty = ((startPt[1]<endPt[1])?(startPt[1]-miny):(maxy-startPt[1]))/fabs(endPt[1]-startPt[1]);
	float minz = gridSize*floor(startPt[2]/gridSize), maxz = minz + gridSize;
	float tz = ((startPt[2]<endPt[2])?(startPt[2]-minz):(maxz-startPt[2]))/fabs(endPt[2]-startPt[2]);

	float delta_tx = gridSize/fabs(endPt[0]-startPt[0]);
	float delta_ty = gridSize/fabs(endPt[1]-startPt[1]);
	float delta_tz = gridSize/fabs(endPt[2]-startPt[2]);

	Vec3d origin(startPt[0],startPt[1],startPt[2]);
	Vec3d dest(endPt[0],endPt[1],endPt[2]);
	float vt,wt;
	
	while(1)
	{
		//printf("Processing Cell(%d,%d,%d)...", i,j,k);
		std::pair<multi_map_tri::iterator, multi_map_tri::iterator> r = x.equal_range(Cell(i,j,k));
		for(multi_map_tri::iterator it = r.first; it != r.second; it++)
			{
				Vec3d v0(vertices[3*(it->second->index[0])],vertices[3*(it->second->index[0])+1],vertices[3*(it->second->index[0])+2] );
				Vec3d v1(vertices[3*(it->second->index[1])],vertices[3*(it->second->index[1])+1],vertices[3*(it->second->index[1])+2] );
				Vec3d v2(vertices[3*(it->second->index[2])],vertices[3*(it->second->index[2])+1],vertices[3*(it->second->index[2])+2] );
				
				if(testSegmentTriangle<Vec3d>(origin, dest, v0, v1, v2, vt, wt))
				{

					idx1 = it->second->index[0];
					idx2 = it->second->index[1];
					idx3 = it->second->index[2];
					v = vt; w = wt;
					return true;
				}
				
			}
		if(tx <= ty && tx <= tz)
		{
			if (i == iend) break;
			tx += delta_tx;
			i += di;
		}
		else if (ty <= tx && ty <= tz)
		{
			if(j == jend) break;
			ty += delta_ty;
			j += dj;
		}
		else
		{
			if(k == kend) break;
			tz += delta_tz;
			k += dk;
		}
	}
	return false;

}
