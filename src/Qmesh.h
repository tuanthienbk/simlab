#pragma once
#include <stdio.h>
#include <Eigen/Dense>
#include <Eigen/SparseCore>

class QMesh
{
public:
	int nF,nV;
	std::vector<Eigen::Vector3f> vertices; // vertex list
	std::vector<Eigen::Vector2f> tex_coords; // vertex list
	std::vector<Eigen::Vector3f> normals; // normal list
	std::vector<Eigen::Vector4i> faces; // face list
	std::vector<Eigen::Vector4i> texIndices; // texture list
	std::vector<Eigen::Vector4i> normalIndices; // texture list
	std::vector<int> twin;
	std::vector<int> edge_map;
	std::vector<int> valence;
	std::vector<int> eops;
	int maxValence;
public:
	QMesh() {}
	QMesh(std::vector<Eigen::Vector3f> v, std::vector<Eigen::Vector4i> f): vertices(v), faces(f)
	{
		nF = f.size();
		nV = v.size();
		std::vector<Eigen::Triplet<int>> tripletList;
		tripletList.reserve(4*nF);
		for(int idx = 0; idx < nF; idx++) {
			Eigen::Vector4i& vertexIndex = f[idx];
			tripletList.push_back(Eigen::Triplet<int>(vertexIndex[0],vertexIndex[1],4*idx+1));
			tripletList.push_back(Eigen::Triplet<int>(vertexIndex[1],vertexIndex[2],4*idx+2));
			tripletList.push_back(Eigen::Triplet<int>(vertexIndex[2],vertexIndex[3],4*idx+3));
			tripletList.push_back(Eigen::Triplet<int>(vertexIndex[3],vertexIndex[0],4*idx+4));
		}
		Eigen::SparseMatrix<int> edges(nV,nV);
		edges.setFromTriplets(tripletList.begin(), tripletList.end());
		buildHalfEdge(edges);
		computeValence();
	}
	void loadOBJ(const char* path)
	{
		FILE * file = fopen(path, "r");
		if( file == NULL ){
			printf("Impossible to open the file ! Are you in the right path ? \n");
			getchar();
			return;
		}
		std::vector<Eigen::Triplet<int>> tripletList;
		tripletList.reserve(4000); // about 1000 faces
		int idx = 0;
		while( 1 ){
			char lineHeader[128];
			// read the first word of the line
			int res = fscanf(file, "%s", lineHeader);
			if (res == EOF)
				break; // EOF = End Of File. Quit the loop.

			if ( strcmp( lineHeader, "v" ) == 0 ){
				Eigen::Vector3f vertex;
				fscanf(file, "%f %f %f\n", &vertex[0], &vertex[1], &vertex[2] );
				vertices.push_back(vertex);
			}else if ( strcmp( lineHeader, "vt" ) == 0 ){
				Eigen::Vector2f uv;
				fscanf(file, "%f %f\n", &uv[0], &uv[1] );
				uv[1] = -uv[1]; // Invert V coordinate since we will only use DDS texture, which are inverted. Remove if you want to use TGA or BMP loaders.
				tex_coords.push_back(uv);
			}else if ( strcmp( lineHeader, "vn" ) == 0 ){
				Eigen::Vector3f  normal;
				fscanf(file, "%f %f %f\n", &normal[0], &normal[1], &normal[2] );
				normals.push_back(normal);
			}else if ( strcmp( lineHeader, "f" ) == 0 ){
				Eigen::Vector4i vertexIndex, uvIndex, normalIndex;
				int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2], &vertexIndex[3], &uvIndex[3], &normalIndex[3] );
				if (matches != 12){
					printf("File can't be read by our simple parser :-( Try exporting with other options\n");
					getchar();
					return;
				}
				vertexIndex -= Eigen::Vector4i(1,1,1,1);
				uvIndex -= Eigen::Vector4i(1,1,1,1);
				normalIndex -= Eigen::Vector4i(1,1,1,1);
				faces.push_back(vertexIndex);
				texIndices.push_back(uvIndex);
				normalIndices.push_back(normalIndex);

				tripletList.push_back(Eigen::Triplet<int>(vertexIndex[0],vertexIndex[1],4*idx+1));
				tripletList.push_back(Eigen::Triplet<int>(vertexIndex[1],vertexIndex[2],4*idx+2));
				tripletList.push_back(Eigen::Triplet<int>(vertexIndex[2],vertexIndex[3],4*idx+3));
				tripletList.push_back(Eigen::Triplet<int>(vertexIndex[3],vertexIndex[0],4*idx+4));
				idx++;
			}
			else{
				// Probably a comment, eat up the rest of the line
				char stupidBuffer[1000];
				fgets(stupidBuffer, 1000, file);
			}

		}
		nF = faces.size();
		nV = vertices.size();
		Eigen::SparseMatrix<int> edges(nV,nV);
		edges.setFromTriplets(tripletList.begin(), tripletList.end());
		buildHalfEdge(edges);
		computeValence();
	}
	//! incident face of the edge
	inline int face(int edge) { return edge/4;}
	//! initial vertex of the edge
	inline int initial(int edge) { return faces[face(edge)][edge%4];}
	//! terminal vertex of the edge
	inline int terminal(int edge) { return faces[face(edge)][(edge + 1)%4];}
	//! local index of the edge in the quad
	inline int local(int edge) { return edge%4;}
	//! next edge in the quad
	inline int next(int edge) { return 4*(edge/4) + (edge + 1)%4;}
	//! previous edge in the triangle
	inline int prev(int edge) { return 4*(edge/4) + (edge + 3)%4;}
	//! opposite edge (twin edge)
	inline int opposite(int edge) {return twin[edge];}
	//! get edge index from vertex index
	inline int edge(int vertex) { return edge_map[vertex];}
	//! get edge index from vertex index
	int edge(int face, int vertex) {
		for(int i = 0; i < 4; i++)
			if (faces[face][i] == vertex)
				return 4*face + i;
		return -1;
	}
	//! test the edge if it is on boundary
	inline bool isBoundaryEdge(int edge) {return twin[edge] < 0;}
	//! test the vertex if it is on boundary
	inline bool isBoundaryVertex(int vertex) {return isBoundaryEdge(edge(vertex));}
	//!test the face if it is a boundary face
	inline bool isBoundaryFace(int face) {
		for(int i = 0; i < 4; i++)
			if (isBoundaryEdge(4*face+i)) return true;
		return false;
	}
	//!test the if it contains an extra-ordinary point
	inline bool isExtraOrdinaryFace(int face)
	{
		if (isBoundaryFace(face)) return false;
		for(int i = 0; i < 4; i++)
			if (valence[faces[face][i]] != 4) return true;
		return false;
	}
	//! test the edge if it is an inner edge
	inline bool isInnerEdge(int edge) {return twin[edge] >= 0;}
	//! get valence or degree of a vertex
	inline int getValence(int vi) {	return valence[vi];	}

	//! Find vertex list of a face ordered by subdivision rule
	void getSubDivMesh(int fi, std::vector<int> &orv)
	{
		int i,n,v;

		for(i = 0; i < 4; i++) {
			v = faces[fi][i];
			n = valence[v];
			if ( n != 4) break ;
		}
		if (n ==4 ) v = faces[fi][0];

		orv.push_back(v);
		int e = edge(fi,v);
		for(int k = 0; k < n; k++)
		{
			orv.push_back(terminal(e));
			orv.push_back(terminal(next(e)));
			e = opposite(prev(e));
		}
		e = next(opposite(prev(opposite(e))));
		orv.push_back(terminal(e));
		e = opposite(next(next(e)));
		orv.push_back(terminal(e));
		e = opposite(next(next(e)));
		orv.push_back(terminal(e));
		e = next(e);
		orv.push_back(terminal(e));
		e = next(e);
		orv.push_back(terminal(e));
		e = next(opposite(next(e)));
		orv.push_back(terminal(e));
		e = next(opposite(next(e)));
		orv.push_back(terminal(e));
		return;
	}
	//! Generate refine ring aroud extra-ordinary point
	QMesh* generateSubDivRing(int vt)
	{
		int n = valence[vt];
		std::vector<Eigen::Vector3f> rv(12*n+1);
		std::vector<Eigen::Vector4i> rf(9*n);
		std::vector<int> tr = tworingv(vt);
//		for(int i = 0 ; i < tr.size(); i++)
//			std::cout << tr[i] << " ";
//		std::cout << std::endl;
		rv[0] = (4*n*n - 7*n)*vertices[vt];
		for(int k = 0; k < n; k++)
		{
			rv[12*k+1] = (6*vertices[vt] + 6*vertices[tr[6*k+1]] + vertices[tr[6*k+2]]
						+ vertices[tr[6*((k+1)%n)+1]] + vertices[tr[6*((k-1+n)%n)+1]] + vertices[tr[6*((k-1+n)%n)+2]])/16;
			rv[12*k+2] = (6*vertices[vt] + 36*vertices[tr[6*k+1]] + 6*vertices[tr[6*k+2]] + vertices[tr[6*((k+1)%n)+1]]
						 + vertices[tr[6*((k-1+n)%n)+1]] + 6*vertices[tr[6*((k-1+n)%n)+2]] + vertices[tr[6*((k-1+n)%n)+6]]
					     + 6*vertices[tr[6*k+3]] + vertices[tr[6*k+4]])/64;
			rv[12*k+3] = (vertices[tr[6*((k-1+n)%n)+2]] + vertices[tr[6*((k-1+n)%n)+6]] + 6*vertices[tr[6*k+1]] + 6*vertices[tr[6*k+3]]
						 + vertices[tr[6*k+2]] + vertices[tr[6*k+4]])/16;
			rv[12*k+4] = (vertices[vt] + vertices[tr[6*k+1]] + vertices[tr[6*k+2]] + vertices[tr[6*((k+1)%n)+1]])/4;
			rv[12*k+5] = (vertices[vt] + 6*vertices[tr[6*k+1]] + 6*vertices[tr[6*k+2]] + vertices[tr[6*((k+1)%n)+1]]
						  + vertices[tr[6*k+3]] + vertices[tr[6*k+4]])/16;
			rv[12*k+6] = (vertices[tr[6*k+1]] + vertices[tr[6*k+2]] + vertices[tr[6*k+3]] + vertices[tr[6*k+4]])/4;
			rv[12*k+7] = (vertices[vt] + vertices[tr[6*k+1]] + 6*vertices[tr[6*k+2]] + 6*vertices[tr[6*((k+1)%n)+1]]
						  + vertices[tr[6*k+6]] + vertices[tr[6*((k+1)%n)+3]])/16;
			rv[12*k+8] = (vertices[vt] + 6*vertices[tr[6*k+1]] + 36*vertices[tr[6*k+2]] + 6*vertices[tr[6*((k+1)%n)+1]]
					  + 6*vertices[tr[6*k+6]] + vertices[tr[6*((k+1)%n)+3]] + vertices[tr[6*k+3]]
						+ 6*vertices[tr[6*k+4]] + vertices[tr[6*k+5]])/64;
			rv[12*k+9] = (vertices[tr[6*k+1]] + vertices[tr[6*k+3]] + 6*vertices[tr[6*k+2]]
						+ 6*vertices[tr[6*k+4]] + vertices[tr[6*k+5]] + vertices[tr[6*k+6]])/16;
			rv[12*k+10] = (vertices[tr[6*((k+1)%n)+1]] + vertices[tr[6*k+6]] + vertices[tr[6*k+2]] + vertices[tr[6*((k+1)%n)+3]])/4;
			rv[12*k+11] = (vertices[tr[6*((k+1)%n)+1]] + 6*vertices[tr[6*k+6]] + 6*vertices[tr[6*k+2]] + vertices[tr[6*((k+1)%n)+3]]
					   + vertices[tr[6*k+4]] + vertices[tr[6*k+5]])/16;
			rv[12*k+12] = (vertices[tr[6*k+2]] + vertices[tr[6*k+6]]+ vertices[tr[6*k+4]] + vertices[tr[6*k+5]])/4;
			rv[0] += 6*vertices[tr[6*k+1]] + vertices[tr[6*k+2]];

			rf[9*k] << 0,12*k+1,12*k+4,12*((k+1)%n)+1;
			rf[9*k+1] << 12*k+1,12*k+2,12*k+5,12*k+4;
			rf[9*k+2] << 12*k+2,12*k+3,12*k+6,12*k+5;
			rf[9*k+3] << 12*((k+1)%n)+1,12*k+4,12*k+7,12*((k+1)%n)+2;
			rf[9*k+4] << 12*k+4,12*k+5,12*k+8,12*k+7;
			rf[9*k+5] << 12*k+5,12*k+6,12*k+9,12*k+8;
			rf[9*k+6] << 12*((k+1)%n)+2,12*k+7,12*k+10,12*((k+1)%n)+3;
			rf[9*k+7] << 12*k+7,12*k+8,12*k+11,12*k+10;
			rf[9*k+8] << 12*k+8,12*k+9,12*k+12,12*k+11;
		}
		rv[0] /= 4*n*n;
		return new QMesh(rv,rf);
	}
	//! Find 2-ring of a vertex
	std::vector<int> tworingv(int vt)
	{
		int n = valence[vt];
		std::vector<int> rs(6*n+1);
		rs[0] = vt;
		int e = edge(vt);
		e = next(e);
		for(int k = 0; k < n; k++)
		{
			rs[6*k+1] = terminal(e);
			int ee = next(e);
			rs[6*k+2] = terminal(ee);
			ee = next(opposite(ee));
			rs[6*k+3] = terminal(ee);
			ee = next(ee);
			rs[6*k+4] = terminal(ee);
			ee = next(opposite(next(ee)));
			rs[6*k+5] = terminal(ee);
			ee = next(ee);
			rs[6*k+6] = terminal(ee);
			e = opposite(prev(e));
		}
		return rs;
	}
	//! Find 1-ring of the vertex with index vi
	void oneringv(int vi, std::vector<int> &orv)
	{
		int first_edge = edge(vi);
		int next_edge = first_edge;
		while (1)
		{
			orv.push_back(initial(next_edge));
			next_edge = next(next_edge);
			if (isBoundaryEdge(next_edge)) {
				orv.push_back(terminal(next_edge));
				break;
			}
			next_edge = opposite(next_edge);
			if (next_edge == first_edge) break;
		}

	}
	//! Find 1-ring face of the vertex
	void oneringf(int vi, std::vector<int> &orf)
	{
		int first_edge = edge(vi);
		int next_edge = first_edge;
		while (1)
		{
			orf.push_back(face(next_edge));
			next_edge = next(next_edge);
			if (isBoundaryEdge(next_edge)) break;
			next_edge = opposite(next_edge);
			if (next_edge == first_edge) break;
		}
	}
private:
	void buildHalfEdge(Eigen::SparseMatrix<int> &edges)
	{
		twin.resize(4*nF);
		edge_map.resize(nV);
		for (int i=0; i < nV; ++i)
		{
			Eigen::SparseMatrix<int>::InnerIterator it(edges,i);
			edge_map[i] = it.value() - 1;
			for(;it;++it)
			{
				int ie = edges.coeff(it.col(),it.row())-1;
				twin[it.value()-1] = ie;
				if (ie < 0) edge_map[i] = it.value() - 1;

			}

		}
	}
	void computeValence()
	{
		std::map<int,int> nelems;
		for (int i=0; i < nF; i++)
		{
			for (unsigned int j=0; j<4; j++)
			{
				++nelems[faces[i][j]];
			}
		}
		maxValence = 0;
		valence.resize(nV);
		for (auto it = nelems.begin(); it != nelems.end(); ++it)
		{
			valence[it->first] = it->second;
			if (it->second != 4 && !isBoundaryVertex(it->first))
				eops.push_back(it->first);
			if (it->second > maxValence)
				maxValence = it->second;
		}
	}
};
