#include "volumetricMeshLoader.h"
#include "corotationalLinearFEM.h"
#include "generateMassMatrix.h"
#include "implicitBackwardEulerSparse.h"
#include "corotationalLinearFEM.h"
#include "corotationalLinearFEMMT.h"
#include "corotationalLinearFEMForceModel.h"
#include "linearFEMForceModel.h"
#include "isotropicHyperelasticFEM.h"
#include "isotropicHyperelasticFEMMT.h"
#include "isotropicHyperelasticFEMForceModel.h"
#include "isotropicMaterial.h"
#include "StVKIsotropicMaterial.h"
#include "neoHookeanIsotropicMaterial.h"
#include "MooneyRivlinIsotropicMaterial.h"
#include "objMesh.h"
#include "generateSurfaceMesh.h"
#include "objMeshRender.h"
#include "StVKCubeABCD.h"
#include "StVKTetABCD.h"
#include "StVKTetHighMemoryABCD.h"
#include "StVKInternalForces.h"
#include "StVKStiffnessMatrix.h"
#include "StVKInternalForcesMT.h"
#include "StVKStiffnessMatrixMT.h"
#include "StVKForceModel.h"
#include "StVKElementABCDLoader.h"
#include "ImplicitConstraints.h"

class VolumetricFEM
{
public:
    ObjMesh* bdySurface;
    VolumetricMesh* mesh;
    
    // (tangential) Rayleigh damping
    double dampingMassCoef;// = 0.0; // "underwater"-like damping (here turned off)
    double dampingStiffnessCoef;// = 0.01; // (primarily) high-frequency damping
    double time_step;
    std::vector<int> constrainedDOFs;
    std::vector<bool> constrainedVertices;
    
    SparseMatrix * massMatrix;

    int nNode; // number of nodes
    SurfLab::Constraints::CylinderContraints constraints;
private:

    ForceModel * forceModel;
    IntegratorBaseSparse *solver;
    ObjMeshRender* visualizer;
    double *q,*q_vel,*q_acc;
public:
    VolumetricFEM() {}

    void createModel(const char* filename)
    {
        mesh = VolumetricMeshLoader::load((char*)filename);
        if (mesh == NULL)
            printf("Error: failed to load mesh.\n");
        else
            printf("Success. Number of vertices: %d . Number of elements: %d .\n", mesh->getNumVertices(), mesh->getNumElements());
        
        if (mesh->getElementType() != VolumetricMesh::TET)
        {
            printf("Error: not a tet mesh.\n");
            exit(1);
        }
        nNode = mesh->getNumVertices();

        bdySurface = GenerateSurfaceMesh::ComputeMesh(mesh,true);
        bdySurface->buildVertexFaceNeighbors();
        bdySurface->initTriangleLookup();
        visualizer = new  ObjMeshRender(bdySurface);
        // displayListObj = render.createDisplayList(OBJMESHRENDER_TRIANGLES, OBJMESHRENDER_SMOOTH);

        GenerateMassMatrix::computeMassMatrix(mesh, &massMatrix, true);

//        IsotropicMaterial * isotropicMaterial = new StVKIsotropicMaterial(mesh, true, 10.0);
//        IsotropicHyperelasticFEM * isotropicHyperelasticFEM = new IsotropicHyperelasticFEMMT(mesh, isotropicMaterial, 0.1, false, 0.0, 8);
//        forceModel = new IsotropicHyperelasticFEMForceModel(isotropicHyperelasticFEM);

        CorotationalLinearFEM * deformableModel = new CorotationalLinearFEMMT((TetMesh*)mesh,32);
        forceModel = new CorotationalLinearFEMForceModel(deformableModel);

        constrainedVertices.resize(nNode,0);
        for(int i = 0;i<nNode;i++)
        {
          double y = (*mesh->getVertex(i))[1];
			    if (y >=0.5 || y <=-0.2)
			    {
				    setFixedVertex(i);
				    constrainedDOFs.push_back(3*i);
				    constrainedDOFs.push_back(3*i+1);
				    constrainedDOFs.push_back(3*i+2);
			    }
        }

        int r = 3 * nNode;
        int positiveDefiniteSolver = 0;

        time_step = 1e-3;
        dampingMassCoef = .1;
        dampingStiffnessCoef  = 0.001;
        solver = new ImplicitBackwardEulerSparse(r, time_step, massMatrix, forceModel,
                positiveDefiniteSolver, constrainedDOFs.size(), constrainedDOFs.data(), dampingMassCoef, dampingStiffnessCoef,1,1e-6,32);

		    q = new double[r];
		    q_vel = new double[r];
		    q_acc = new double[r]; 
    }
    void createHexModel(const char* volname, const char* surfname)
    {
        mesh = VolumetricMeshLoader::load((char*)volname);
        if (mesh == NULL)
            printf("Error: failed to load mesh.\n");
        else
            printf("Success. Number of vertices: %d . Number of elements: %d .\n", mesh->getNumVertices(), mesh->getNumElements());

        nNode = mesh->getNumVertices();

        bdySurface = new ObjMesh((char*)surfname,0);

        bdySurface->buildVertexFaceNeighbors();
        visualizer = new ObjMeshRender(bdySurface);
        GenerateMassMatrix::computeMassMatrix(mesh, &massMatrix, true);

        unsigned int loadingFlag = 1; // 0 = use low-memory version, 1 = use high-memory version
        StVKElementABCD * precomputedIntegrals = StVKElementABCDLoader::load(mesh, loadingFlag);
        if (precomputedIntegrals == NULL)
        {
            printf("Error: unable to load the StVK integrals.\n");
            exit(1);
        }
        int numInternalForceThreads = 32;
        StVKInternalForces* stVKInternalForces = NULL;
        StVKStiffnessMatrix * stVKStiffnessMatrix = NULL;
        StVKForceModel * stVKForceModel = NULL;

        printf("Generating internal forces and stiffness matrix models...\n"); fflush(NULL);
        int addGravity=0;
        double g=9.81;
        if (numInternalForceThreads == 0)
           stVKInternalForces = new StVKInternalForces(mesh, precomputedIntegrals, addGravity, g);
        else
           stVKInternalForces = new StVKInternalForcesMT(mesh, precomputedIntegrals, addGravity, g, numInternalForceThreads);

        if (numInternalForceThreads == 0)
           stVKStiffnessMatrix = new StVKStiffnessMatrix(stVKInternalForces);
        else
           stVKStiffnessMatrix = new StVKStiffnessMatrixMT(stVKInternalForces, numInternalForceThreads);

        stVKForceModel = new StVKForceModel(stVKInternalForces, stVKStiffnessMatrix);
        forceModel = stVKForceModel;

        constrainedVertices.resize(nNode,0);
        for(int i = 0;i<nNode;i++)
        {
			    double y = (*mesh->getVertex(i))[1];
			    if (y >=0.5 || y <=0)
			    {
				    setFixedVertex(i);
				    constrainedDOFs.push_back(3*i);
				    constrainedDOFs.push_back(3*i+1);
				    constrainedDOFs.push_back(3*i+2);
			    }
        }

        int r = 3 * nNode;
        int positiveDefiniteSolver = 0;

        time_step = 1e-3;
        dampingMassCoef = 1.0;
        dampingStiffnessCoef  = 0.001;
        solver = new ImplicitBackwardEulerSparse(r, time_step, massMatrix, forceModel,
                positiveDefiniteSolver, constrainedDOFs.size(), constrainedDOFs.data(), dampingMassCoef, dampingStiffnessCoef,1,1e-6,32);

       
        /*disp = solver->Getq();
        vel = solver->Getqvel();
        accel = solver->Getqaccel();*/
    }
    void doTimeStep(double elapsed_time)
    {
        solver->GetqState(q,q_vel,q_acc);

//        int nStep = (int)(elapsed_time/time_step);
//        for(int k=0; k < 1; k++)
        double *pos,*vel;

//        static bool checked = false;
//        for(;;)
        solver->DoTimestep();
        pos = solver->Getq();
        vel = solver->Getqvel();
        int numberOfCollisions = 0;
        if (numberOfCollisions == 0)
        {

//#pragma omp parallel for shared(flag)
            for(int idx = 0; idx < nNode; idx++)
            {
                if (numberOfCollisions > 0) break;
                if (!isConstrainedAtVertex(idx))
                {
                    Vec3d *vertex = mesh->getVertex(idx);
                    Eigen::Vector3d P((*vertex)[0]+pos[3*idx],(*vertex)[1]+pos[3*idx+1],(*vertex)[2]+pos[3*idx+2]);
                    Eigen::Vector3d Q((*vertex)[0]+q[3*idx],(*vertex)[1]+q[3*idx+1],(*vertex)[2]+q[3*idx+2]);
                    numberOfCollisions += constraints.test_large_disp(P,Q);

                }

            }

        }
        if (numberOfCollisions > 0)
        {
            time_step *= 1e-1;
            qDebug() << time_step <<"\n";
            solver->SetTimestep(time_step);
            solver->SetqState(q,q_vel,q_acc);
            solver->DoTimestep();
            pos = solver->Getq();
            vel = solver->Getqvel();

        }
        else
        {
            time_step = 1e-3;
            solver->SetTimestep(time_step);
        }


        // resolving point collision
#pragma omp parallel for		
        for(int i = 0; i < nNode; i++)
            if (!isConstrainedAtVertex(i))
            {
                Vec3d *vertex = mesh->getVertex(i);
                Eigen::Vector3d P((*vertex)[0]+pos[3*i],(*vertex)[1]+pos[3*i+1],(*vertex)[2]+pos[3*i+2]);
                constraints.resolve_point(P,&pos[3*i],&vel[3*i]);
            }
			
		// resolving triangle collision
//		for(int i = 0; i < bdySurface->getNumFaces(); i++)
//		{
//			int v0_idx = bdySurface->getVertexIndex(0,i,0);
//			int v1_idx = bdySurface->getVertexIndex(0,i,1);
//			int v2_idx = bdySurface->getVertexIndex(0,i,2);
//			Vec3d *v0 = mesh->getVertex(v0_idx);
//			Vec3d *v1 = mesh->getVertex(v1_idx);
//			Vec3d *v2 = mesh->getVertex(v2_idx);
//			Eigen::Vector3d A((*v0)[0] + q[3*v0_idx],(*v0)[1] + q[3*v0_idx+1],(*v0)[2] + q[3*v0_idx+2]);
//			Eigen::Vector3d B((*v1)[0] + q[3*v1_idx],(*v1)[1] + q[3*v1_idx+1],(*v1)[2] + q[3*v1_idx+2]);
//			Eigen::Vector3d C((*v2)[0] + q[3*v2_idx],(*v2)[1] + q[3*v2_idx+1],(*v2)[2] + q[3*v2_idx+2]);

//			constraints.resolve_triangle(A,B,C,&q[v0_idx],&q[v1_idx],&q[v2_idx]);
//		}
		// update surface position
#pragma omp parallel for		
        for(int i = 0; i < bdySurface->getNumVertices(); i++)
			if (!isConstrainedAtVertex(i))
				bdySurface->setPosition(i,*mesh->getVertex(i) + Vec3d(q[3*i],q[3*i+1],q[3*i+2]));
		
    }

    void setFixedVertex(int vid) {	constrainedVertices[vid] = 1; }
    bool isConstrainedAtVertex( int vid ) { return constrainedVertices[vid]; }
    void setUnconstrainedVertex(int vid) { constrainedVertices[vid] = 0; }
    void setPos(int id, double* pos)
    {
        Vec3d *v = mesh->getVertex(id);
		solver->SetQ(3*id,pos[0] - (*v)[0]);
        solver->SetQ(3*id+1,pos[1] - (*v)[1]);
        solver->SetQ(3*id+2,pos[2] - (*v)[2]);
        bdySurface->setPosition(id,Vec3d(pos[0],pos[1],pos[2]));
    }
    void updateConstraints()
	{
		constrainedDOFs.clear();
		for(int i = 0;i<nNode;i++)
        {
			if (isConstrainedAtVertex(i))
			{
				constrainedDOFs.push_back(3*i);
				constrainedDOFs.push_back(3*i+1);
				constrainedDOFs.push_back(3*i+2);
			}
        }

		delete solver;
		solver = new ImplicitBackwardEulerSparse(3*nNode, time_step, massMatrix, forceModel,
                0, constrainedDOFs.size(), constrainedDOFs.data(), dampingMassCoef, dampingStiffnessCoef,1,1e-6,32);
		solver->SetqState(q,q_vel,q_acc);
	}
    void visualize()
    {
        bdySurface->buildFaceNormals();
        bdySurface->buildVertexNormals(85.);
        visualizer->render(OBJMESHRENDER_TRIANGLES
                            | OBJMESHRENDER_EDGES, OBJMESHRENDER_SMOOTH);
    }

};
