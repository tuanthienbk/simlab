#include <Eigen/Core>
#include <Eigen/Geometry>

typedef Eigen::Vector3d Vec3;
typedef Eigen::Vector2d Vec2;
typedef Eigen::Transform<double,3,Eigen::Affine> Transform;

class ImplicitContraints
{
public:
	virtual void resolve(Vec3 &P, Vec3 &vel)=0;
	virtual void resolve(Vec3 &P1, Vec3 &vel1, Vec3 &P2, Vec3 &vel2){};
	virtual void resolve(Vec3 &P1, Vec3 &vel1, Vec3 &P2, Vec3 &vel2, Vec3 &P3, Vec3 &vel3){};
};

class CylinderContraints: public ImplicitContraints
{
public:
	Vec3 O1, O2; // axis = O2 - O1;
	Vec3 c_axis;
	double r;
	double length;
	Transform t;
public:
	CylinderContraints():O1(Vec3(0.2,0.6,-1.0)), O2(Vec3(.3,0.6,1.0)), r(0.1)
	{
		init();
	}
	
	void resolve(Vec3 &P, Vec3 &vel)
	{
		// plane through P perpendicular to the axis is (x-P).c_axis = 0
		// ( (1-alpha)*O1 + alpha*O2 - P). c_axis = 0
		double alpha = (P - O1).dot(c_axis)/length;
		if ( 0 <= alpha && alpha <=  1) // if plane intersects the line O1-O2
		{
			Vec3 O = (1 - alpha)*O1 + alpha*O2; // compute the intersecting point O
			double d = (P-O).norm(); // distance from P to O
			if ( d < r) // if the distance is less than the radius, P is inside
			{
				Vec3 t = (P-O)/d;
				P = O + r*t;
				Vec3 v = vel - (vel.dot(c_axis))*c_axis;
                  v = v - 2*(v.dot(t))*t;
                  vel = v + (vel.dot(c_axis))*c_axis;
			}
		
		}
	}
	
	void init()
	{
		length = (O2-O1).norm();
		c_axis = (O2-O1)/length;
		Vec3 z_axis(0.0,0.0,1.0);
		Vec3 rot = z_axis.cross(c_axis);
		double mag = rot.norm();
		if (mag > 1e-6)
		{
			double theta = atan2(mag,z_axis.dot(c_axis));
			t = Eigen::Translation3d(O1)*Eigen::AngleAxisd(theta, rot/mag);
		}
		else
		{
			t = Eigen::Affine3d::Identity();
		}
	}
      int test_large_disp(Vec3& P, Vec3& Q)
      {
          double alpha = (P - O1).dot(c_axis)/length;
          if ( 0 <= alpha && alpha <=  1) // if plane intersects the line O1-O2
          {
              Vec3 O = (1 - alpha)*O1 + alpha*O2; // compute the intersecting point O
              double d = (P-O).norm(); // distance from P to O
              if ( d < r ) // if the distance is less than the radius, P is inside
                 if ((P-Q).dot(P-Q) > 0.25*r*r)
                      return 1;
          }
          return 0;
      }
	void resolve_point(Vec3& P, double *disp, double* v)
	{
		// plane through P perpendicular to the axis is (x-P).c_axis = 0
		// ( (1-alpha)*O1 + alpha*O2 - P). c_axis = 0
		Eigen::Map<Eigen::Vector3d> Disp(disp);
		Eigen::Map<Eigen::Vector3d> vel(v);
		double alpha = (P - O1).dot(c_axis)/length;
		if ( 0 <= alpha && alpha <=  1) // if plane intersects the line O1-O2
		{
			Vec3 O = (1 - alpha)*O1 + alpha*O2; // compute the intersecting point O
			double d = (P-O).norm(); // distance from P to O
			if ( d < r) // if the distance is less than the radius, P is inside
			{
				Vec3 t = (P-O)/d;
//                    vel += 1000*(r-d)*t;
                  Disp += (r-d)*t;
                  Vec3 vp = vel - (vel.dot(c_axis))*c_axis;
//                    double mag = vp.dot(t);
//                    if (mag < 0)
                      vp = vp + 2*fabs(vp.dot(t))*t;
//                    else vp = 2*mag*t - vp;
                  vel = vp + (vel.dot(c_axis))*c_axis;
			}
		
		}
	}
	double dist2axis(Vec3 P, Vec3 &disp)
	{
		double alpha = (P - O1).dot(c_axis)/length;
		if ( 0 <= alpha && alpha <=  1) // if plane intersects the line O1-O2
		{
			Vec3 O = (1 - alpha)*O1 + alpha*O2; // compute the intersecting point O
			double d = (P-O).norm();
			disp = (P-O)/d;
			return d;
		}
		else return r + 1;
	}
	void resolve_triangle(Vec3& A, Vec3& B, Vec3& C, double* dispA, double* dispB, double* dispC)
	{
		Eigen::Map<Eigen::Vector3d> dA(dispA);
		Eigen::Map<Eigen::Vector3d> dB(dispB);
		Eigen::Map<Eigen::Vector3d> dC(dispC);
		Vec3 disp[4];
		double d[4];
		d[0] = dist2axis(0.33333333*(A+B+C),disp[0]);
		d[1] = dist2axis(0.5*(A+B),disp[1]);
		d[2] = dist2axis(0.5*(C+B),disp[2]);
		d[3] = dist2axis(0.5*(A+C),disp[3]);

		int idx = 0;
		for(int i = 1; i < 4; i++)
			if (d[i] < d[idx]) idx = i;
		if (d[idx] < r)
		{
			dA += (r - d[idx])*disp[idx];
			dB += (r - d[idx])*disp[idx];
			dC += (r - d[idx])*disp[idx];
		}
	}
	
};

