#include "TLEDVolume.h"

namespace SurfLab { namespace Volume {

void TLEDVolume::load(const char* filename)
{
    char ele_fname[50];
    char node_fname[50];
    sprintf(ele_fname,"%s.ele",filename);
    sprintf(node_fname,"%s.node",filename);
    FILE* f = fopen(node_fname,"r");
    if (f == NULL)
    {
        throw std::exception("File not found!");
        return;
    }
    int nV, dump;
    fscanf(f,"%d %d%*[^\n]", &nV, &dump);
    x0.resize(nV);
    float x, y, z;

    for(int i = 0; i < nV; i++)
    {
        fscanf(f,"%d %f %f %f%*[^\n]", &dump, &x, &y, &z);
        x0[i] << double(x), double(y), double(z);
    }
    fclose(f);

    f = fopen(ele_fname,"r");
    int nE;
    fscanf(f,"%d %d%*[^\n]", &nE, &dump);
    tet.resize(nE);
    int tet1, tet2, tet3, tet4;
    for(int i = 0; i < nE; i++)
    {
       fscanf(f,"%d %d %d %d %d%*[^\n]", &dump, &tet1, &tet2, &tet3, &tet4);
       tet[i] << tet1, tet2, tet3, tet4;
    }

}
bool isEquiv(const Eigen::Vector3i& f1, const Eigen::Vector3i& f2)
{
    for(int j = 0;j<3;j++)
    {
        bool equality = false;
        for(int i = 0;i<3;i++)
            if (f2[i] == f1[j]) {equality = true;break;}
        if (!equality) return false;
    }
    return true;
}
void visitFace(std::vector<Eigen::Vector3i>& faceList,Eigen::Vector3i face)
{
    for(auto it = faceList.begin();it != faceList.end();it++)
    if (isEquiv(*it,face) )
    {
        faceList.erase(it);
        return;
    }
    faceList.push_back(face);
}
void TLEDVolume::generateSurfaceMesh()
{
    for (int i=0; i<tet.size(); i++)
    {
        Eigen::Vector4i& e = tet[i];
        Eigen::Vector3i f1,f2,f3,f4;
        f1 << e[0],e[1],e[2];
        f2 << e[1],e[2],e[3];
        f3 << e[2],e[3],e[0];
        f4 << e[3],e[0],e[1];

        visitFace(surf,f1);
        visitFace(surf,f2);
        visitFace(surf,f3);
        visitFace(surf,f4);

    }

}
void TLEDVolume::export2off(const char* filename)
{
	FILE *offfile = fopen(filename,"w");
	fprintf(offfile,"OFF\n");
	fprintf(offfile,"%d %d\n", x0.size(), surf.size());
	for(int i = 0; i < x0.size(); i++)
		fprintf(offfile,"%f %f %f% \n",x0[i][0],x0[i][1],x0[i][2]);
			
	for(int i = 0; i < surf.size(); i++)
		fprintf(offfile,"%d %d %d %d \n",3, surf[i][0],surf[i][1],surf[i][2]);
	fclose(offfile);
}
void TLEDVolume::computeSurfaceNormals()
{
    surfNormals.resize(x0.size(), Vec3(0,0,0));
    for(int i = 0; i < surf.size(); i++)
    {
        Vec3 n = (x[surf[i][1]] - x[surf[i][0]]).cross(x[surf[i][2]] - x[surf[i][0]]);
        for(int j = 0; j < 3; j++)
            surfNormals[surf[i][j]] += n;
    }

#pragma omp parallel for
    for(int i = 0; i < x.size(); i++)
    {
        double len = surfNormals[i].norm();
        if (len > 1e-6) surfNormals[i] /= len;
    }
}
void TLEDVolume::init()
{
	time_step = 1e-5;

    double DhDr[4][3];
    DhDr[0][0] = -1; DhDr[0][1] = -1; DhDr[0][2] = -1;
    DhDr[1][0] = 1;  DhDr[1][1] = 0;  DhDr[1][2] = 0;
    DhDr[2][0] = 0;  DhDr[2][1] = 1;  DhDr[2][2] = 0;
    DhDr[3][0] = 0;  DhDr[3][1] = 0;  DhDr[3][2] = 1;

    int nbElems = tet.size();
	int nbVertex = x0.size();
	x = x0;
	x_prev = x0;
	vel.resize(nbVertex, Vec3(0,0,0));
	constrainedVertices.resize(nbVertex,0);
    generateSurfaceMesh();
    computeSurfaceNormals();
    // Element volume (useful to compute shape function global derivatives)
    Volume.resize(nbElems);
    Dh.resize(nbElems);
    elementForces.resize(nbElems);
	mass.resize(nbVertex,0.0);
    // Stores shape function global derivatives
    double DhDx[4][3];

    for (int i=0; i<nbElems; i++)
    {
        // Compute element volume
        Volume[i] = CompElVolTetra(i);
		
        // Compute shape function global derivatives DhDx (DhDx = DhDr * invJ^T)
        ComputeDhDxTetra(i, DhDr, DhDx);

        for (unsigned int j=0; j<4; j++)
        {
            mass[tet[i][j]] += material.mass_density*Volume[i]/6.0;
            // Store DhDx values in 3 texture data arrays (the 3 columns of the shape function derivatives matrix)
            Dh[i](0,j) = DhDx[j][0];
            Dh[i](1,j) = DhDx[j][1];
            Dh[i](2,j) = DhDx[j][2];

        }
    }
    
    forces.resize(nbVertex , Vec3(0,0,0));
    std::map<int,int> nelems;
    for (int i=0; i<nbElems; i++)
    {
        for (unsigned int j=0; j<4; j++)
        {
            ++nelems[tet[i][j]];
        }
    }
    nbElementPerVertex = 0;
    for (auto it = nelems.begin(); it != nelems.end(); ++it)
    {

        if (it->second > nbElementPerVertex)
        {
            nbElementPerVertex = it->second;
        }
    }
    FCrds.resize(nbVertex*nbElementPerVertex, Eigen::Vector2i(-1,-1));
    int * index = new int[nbVertex];
    memset(index, 0, nbVertex*sizeof(int));
    for (int i=0; i<nbElems; i++)
    {
        Eigen::Vector4i& e = tet[i];

        for (unsigned int j=0; j<4; j++)
        {
            // Force coordinates (slice number and index) for each node
            FCrds[nbElementPerVertex * e[j] + index[e[j]] ][0] = j;
            FCrds[nbElementPerVertex * e[j] + index[e[j]] ][1] = i;

            index[e[j]]++;
        }
    }

}
void TLEDVolume::computeForces()
{
    /**
     * Loop over every element and compute forces at each node of the element
     */

//#pragma omp parallel for
    for(int index = 0; index < tet.size(); index++)
    {
            Eigen::Matrix<double, 4, 3, Eigen::RowMajor> NodeDisp;

            NodeDisp.row(0) = x[tet[index][0]] - x0[tet[index][0]];
            NodeDisp.row(1) = x[tet[index][1]] - x0[tet[index][1]];
            NodeDisp.row(2) = x[tet[index][2]] - x0[tet[index][2]];
            NodeDisp.row(3) = x[tet[index][3]] - x0[tet[index][3]];

            /**
            * Computes the transpose of deformation gradient
            * Transpose of displacement derivatives = transpose(shape function derivatives) * ElementNodalDisplacement
            * Transpose of deformation gradient = transpose of displacement derivatives + identity
            */
            Eigen::Matrix3d XT = Dh[index]*NodeDisp + Eigen::Matrix3d::Identity();

            /**
            * Computes the right Cauchy-Green deformation tensor C = XT*X (in fact we compute only 6 terms since C is symetric)
            */
			Eigen::Matrix3d  C = XT*(XT.transpose());


            /**
            * Computes determinant of X
            */
            double J = XT.transpose().determinant();

            /**
            * Computes second Piola-Kirchoff stress
            */
            double SPK[6];

            Eigen::Matrix3d Ci = C.inverse();

            /// Isotropic
            double J23 = pow(J, -2.0/3.0);   // J23 = J^(-2/3)
            double x1 = J23*material.Mu;
            double x4 = -x1*C.trace()/3.0;
            double K = material.Lambda + 2*material.Mu/3.0;
            double x5 = K*J*(J-1);

            /// Elastic component of the response (isochoric part + volumetric part)
            double SiE11, SiE12, SiE13, SiE22, SiE23, SiE33;
            SiE11 = x4*Ci(0,0) + x1;
            SiE22 = x4*Ci(1,1) + x1;
            SiE33 = x4*Ci(2,2) + x1;
            SiE12 = x4*Ci(0,1);
            SiE23 = x4*Ci(1,2);
            SiE13 = x4*Ci(0,2);

            double SvE11, SvE12, SvE13, SvE22, SvE23, SvE33;
            SvE11 = x5*Ci(0,0);
            SvE22 = x5*Ci(1,1);
            SvE33 = x5*Ci(2,2);
            SvE12 = x5*Ci(0,1);
            SvE23 = x5*Ci(1,2);
            SvE13 = x5*Ci(0,2);

            SPK[0] = SiE11 + SvE11;
            SPK[1] = SiE22 + SvE22;
            SPK[2] = SiE33 + SvE33;
            SPK[3] = SiE12 + SvE12;
            SPK[4] = SiE23 + SvE23;
            SPK[5] = SiE13 + SvE13;

            /// Retrieves the volume
            double Vol = Volume[index];
            SPK[0] *= Vol;
            SPK[1] *= Vol;
            SPK[2] *= Vol;
            SPK[3] *= Vol;
            SPK[4] *= Vol;
            SPK[5] *= Vol;


        /**
         * Computes strain-displacement matrix and writes the result in global memory
         */
            double BL[6];
            double Dh0, Dh1, Dh2;
            for (int j = 0; j < 4; j++)
            {
                Dh0 = Dh[index](0,j);
                Dh1 = Dh[index](1,j);
                Dh2 = Dh[index](2,j);
                for(int k = 0; k < 3; k++)
                {
                    BL[0] = Dh0 * XT(0,k);
                    BL[1] = Dh1 * XT(1,k);
                    BL[2] = Dh2 * XT(2,k);
                    BL[3] = Dh1 * XT(0,k) + Dh0 * XT(1,k);
                    BL[4] = Dh2 * XT(1,k) + Dh1 * XT(2,k);
                    BL[5] = Dh2 * XT(0,k) + Dh0 * XT(2,k);
                    elementForces[index](j,k) = SPK[0]*BL[0] + SPK[1]*BL[1] + SPK[2]*BL[2] + SPK[3]*BL[3] + SPK[4]*BL[4] + SPK[5]*BL[5];
                }

            }

    }
    /** Loop over every node and gather forces
     *
     */
 #pragma omp parallel for
    for(int index = 0; index < x0.size(); index++)
    {
        forces[index] << 0.0, 0.0, 0.0 ;
        for (int val=0; val < nbElementPerVertex; val++)
        {
            int nd = nbElementPerVertex*index+val;
            Eigen::Vector2i& FCoord = FCrds[nd];
            if (FCoord[1] >= 0)
                forces[index] += elementForces[FCoord[1]].row(FCoord[0]);
            else
                break;
        }
    }

}
 void TLEDVolume::doTimeStep(double elapsed_time)
 {
	int nStep = (int)(elapsed_time/time_step);
	
	// Verlet's explicit integration
    
	for(int k= 0; k < nStep; k++)
    {
        #pragma omp parallel for
		for(int i = 0; i<x0.size(); i++)
            if (!isConstrainedAtVertex(i))
            {
                // Step 1
                vel[i] = vel[i] + 0.5*time_step*(/*Vec3(0,-9.8,0) +*/ (-forces[i] - material.damping*vel[i])/mass[i]);
                // Step 2
                x_prev[i] = x[i];
                x[i] += vel[i]*time_step;
            }
        // Step 3
        computeForces();

        #pragma omp parallel for
		for(int i = 0; i<x0.size(); i++)
            if (!isConstrainedAtVertex(i))
            {
                // Step 4
                vel[i] = vel[i] + 0.5*time_step*(/*Vec3(0,-9.8,0) +*/ (-forces[i] - material.damping*vel[i])/mass[i]);
                // Step 5
                x[i] = x_prev[i] + vel[i]*time_step;
                // Enforcement of constraints
                // constraints.resolve(x[i],vel[i]);

            }
    }

    // update surface normals
    computeSurfaceNormals();

 }
// --------------------------------------------------------------------------------------
// Computes element volumes for tetrahedral elements
// --------------------------------------------------------------------------------------
double TLEDVolume::CompElVolTetra(int i)
{

    Vec3 v1 = x0[tet[i][1]] - x0[tet[i][0]];
    Vec3 v2 = x0[tet[i][2]] - x0[tet[i][0]];
    Vec3 v3 = x0[tet[i][3]] - x0[tet[i][0]];

    return fabs(v1.cross(v2).dot(v3))/6.0;
}


// -----------------------------------------------------------------------------------------------
// Computes shape function global derivatives DhDx for tetrahedral elements (DhDx = DhDr * invJ^T)
// -----------------------------------------------------------------------------------------------
void TLEDVolume::ComputeDhDxTetra(int i, double DhDr[4][3], double DhDx[4][3])
{
    // Compute Jacobian
    double J[3][3];
    for (int j = 0; j < 3; j++)
    {
        for (int k = 0; k < 3; k++)
        {
            J[j][k] = 0;
            for (unsigned int m = 0; m < 4; m++)
            {
                J[j][k] += DhDr[m][j]*x0[tet[i][m]][k];
            }
        }
    }

    // Jacobian determinant
    double detJ = J[0][0]*(J[1][1]*J[2][2] - J[1][2]*J[2][1]) +
                 J[1][0]*(J[0][2]*J[2][1] - J[0][1]*J[2][2]) +
                 J[2][0]*(J[0][1]*J[1][2] - J[0][2]*J[1][1]);

    // Jacobian inverse
    double invJ[3][3];
    invJ[0][0] = (J[1][1]*J[2][2] - J[1][2]*J[2][1])/detJ;
    invJ[0][1] = (J[0][2]*J[2][1] - J[0][1]*J[2][2])/detJ;
    invJ[0][2] = (J[0][1]*J[1][2] - J[0][2]*J[1][1])/detJ;
    invJ[1][0] = (J[1][2]*J[2][0] - J[1][0]*J[2][2])/detJ;
    invJ[1][1] = (J[0][0]*J[2][2] - J[0][2]*J[2][0])/detJ;
    invJ[1][2] = (J[0][2]*J[1][0] - J[0][0]*J[1][2])/detJ;
    invJ[2][0] = (J[1][0]*J[2][1] - J[1][1]*J[2][0])/detJ;
    invJ[2][1] = (J[0][1]*J[2][0] - J[0][0]*J[2][1])/detJ;
    invJ[2][2] = (J[0][0]*J[1][1] - J[0][1]*J[1][0])/detJ;


    // Compute shape function global derivatives
    for (int j = 0; j < 4; j++)
    {
        for (int k = 0; k < 3; k++)
        {
            DhDx[j][k] = 0;
            for (int m = 0; m < 3; m++)
            {
                DhDx[j][k] += (double)(DhDr[j][m]*invJ[k][m]);
            }
        }
    }
}

}
}
