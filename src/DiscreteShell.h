#include "Tmesh.h"
#include "Materials.h"
#include "ImplicitConstraints.h"

#define IMPLICIT 0

#if IMPLICIT
#include "linear_solver.h"
#endif

	class DiscreteShell
	{
		typedef Eigen::Vector3d Vec3;
	public:
		SurfLab::Mesh::TriangularMesh mesh;
		SurfLab::Material::DiscreteShellMaterial material;
		SurfLab::Constraints::CylinderContraints constraints;
		std::vector<Vec3> vel; // velocities
		std::vector<Vec3> forces;
		std::vector<double> mass; // mass at each node
		std::vector<bool> constrainedVertices;
        std::vector<Vec3> vp; // vertex list
        double time_step;
		
	public:
		DiscreteShell(){}
		DiscreteShell(const char* mesh_off_file)
		{
			createShell(mesh_off_file);
		}
		DiscreteShell(SurfLab::Mesh::TriangularMesh &m, SurfLab::Material::DiscreteShellMaterial &mat): mesh(m), material(mat)
		{
            init();
			
		}
		void setFixedVertex(int vid) {	constrainedVertices[vid] = 1; }
		bool isConstrainedAtVertex( int vid ) { return constrainedVertices[vid]; }
		void setUnconstrainedVertex(int vid) { constrainedVertices[vid] = 0; }
		
		void createShell(const char* mesh_off_file)
		{
			mesh.readOff(mesh_off_file);
			init();
		}
		void doTimeStep(double elapsedTime)
		{
#if IMPLICIT
			double beta = 0.25;
			double lambda = 0.5;
            newmark(elapsedTime, beta, lambda);
#else
            verlet(elapsedTime);
#endif
		}
		//! Mass at each vertex is a third of total area of the incident triangles scaled by mass density
		void computeLumpedMass()
		{
			mass.resize(mesh.nV);
			for(int i = 0; i < mesh.nV; i++)
			{
				mass[i] = 0.0;
				std::vector<int> neighbor_face;
				mesh.oneringf(i,neighbor_face);
				for(int j = 0; j < neighbor_face.size(); j++)
					mass[i] += mesh.getFaceArea(neighbor_face[j]);
				mass[i] *= 0.33333333*material.mass_density;
				
			}
		}
		void updateNormals()
		{
			mesh.normals.resize(mesh.nV);
			for(int i = 0; i < mesh.nF; i++)
				for(int j = 0; j < 3; j++)
					mesh.normals[mesh.f[i][j]] += face_normals[i];
			for(int i = 0; i < mesh.nV; i++)
				mesh.normals[i].normalize();
		}
		void computeForces()
		{
#pragma omp parallel for
			for(int i = 0; i < mesh.nF; i++)
			{
				face_normals[i] = mesh.getFaceAreaVec(i);
				face_area[i] = 0.5*face_normals[i].norm();
				face_normals[i].normalize();
			}

#pragma omp parallel for			
			for(int edge = 0; edge < 3*mesh.nF; edge++)
			{
				int j = mesh.initial(edge);
				int i = mesh.terminal(edge);
				edge_vector[edge] = mesh.v[j] - mesh.v[i];
				edge_length[edge] = edge_vector[edge].norm();
				if (mesh.isInnerEdge(edge) && j < i)
				{
					int f1 = mesh.face(edge);
					int f2 = mesh.face(mesh.opposite(edge));
					dihedral_angle[edge] = atan2(face_normals[f2].cross(face_normals[f1]).dot(edge_vector[edge]), edge_length[edge]*face_normals[f2].dot(face_normals[f1]));
				}
			}

#pragma omp parallel for
			for(int i = 0; i < mesh.nV; i++)
			{
				forces[i] << 0.0f , 0.0f, 0.0f ;
				int first_edge = mesh.edge(i);
				int edge = first_edge;
				for (;;)
				{
					int face = mesh.face(edge); // the face associated with this edge
					int j = mesh.initial(edge);
					//compute edge force (stretching force)
					Vec3& e_ij = edge_vector[edge];
					forces[i] += 2*(material.kE*(edge_length[edge]- rest_edge_length[edge])/(edge_length[edge]*rest_edge_length[edge]))*e_ij;
					
					//compute area force (stretching force)
					int k = mesh.op_vertex(edge); // the remaining vertex in this face
					int edge_kj = mesh.prev(edge);
					int edge_jk = mesh.opposite(edge_kj);
					Vec3& e_jk = edge_vector[edge_kj];
					forces[i] += 2*material.kA*(1- face_area[face]/rest_face_area[face])*e_jk.cross(face_normals[face]);
					
					//compute angle force w.r.t edge k -> j (bending force)
					if (edge_jk >= 0)
					{
						int face_jk = mesh.face(edge_jk);
						double theta = (j < k)?dihedral_angle[edge_jk]:dihedral_angle[edge_kj];
						double A_bar = rest_face_area[face] + rest_face_area[face_jk];
						forces[i] += (material.kTheta*6*rest_edge_length[edge_jk]*(theta - rest_dihedral_angle[edge_jk])*edge_length[edge_jk]/(A_bar*2*face_area[face]))*face_normals[face];
					}
					//compute angle force w.r.t edge j -> i (bending force)
					int edge_ij = mesh.opposite(edge);
					if (edge_ij >= 0)
					{
						int face_ij = mesh.face(edge_ij);
						double theta = (j < i)?dihedral_angle[edge]:dihedral_angle[edge_ij];
						double A_bar = rest_face_area[face] + rest_face_area[face_ij];
						Vec3& e_kkj =  edge_vector[mesh.next(edge_ij)];
						
						forces[i] += (material.kTheta*6*rest_edge_length[edge_ij]*(theta - rest_dihedral_angle[edge_ij])/A_bar)*( -e_kkj.dot(e_ij)/(edge_length[edge]*2*face_area[face_ij])*face_normals[face_ij] + e_jk.dot(e_ij)/(edge_length[edge]*2*face_area[face])*face_normals[face]);
					}
					edge = mesh.next(edge);
					if (mesh.isBoundaryEdge(edge) )
					{
						forces[i] += (2*material.kE*(rest_edge_length[edge] - edge_length[edge])/(edge_length[edge]*rest_edge_length[edge]))*edge_vector[edge];
						break;
					}
					
					edge = mesh.opposite(edge);

					if (edge == first_edge) break;

				}
			}

		}
	private:
		std::vector<double> rest_face_area; // area of the original mesh
		std::vector<double> rest_edge_length; // edge length of the original mesh
		std::vector<double> rest_dihedral_angle; // dihedral angle at each edge

		std::vector<double> dihedral_angle;
		std::vector<Vec3> edge_vector;
		std::vector<double> edge_length;
		std::vector<Vec3> face_normals;
		std::vector<double> face_area;
#if IMPLICIT
		//std::vector<Eigen::Triplet<double> > tripletList;
		std::vector<std::vector<std::pair<int,int> > > vring;
		std::vector<double> val;
		std::vector<int> colPtr;
		std::vector<int> rowIdx;
		
		std::vector<Eigen::Matrix<double, 9, 9> > fHess; // Hessian matrices for each face
        std::vector<Eigen::Matrix<double, 3, 3> > fForces; // Forces correspond to each face

#endif

	private:
        //! verlet integrator
        void verlet(double elapsed_time)
        {
            // x is the position of our nodes
            std::vector<Vec3> &x = mesh.v;
            // xp hold the previous positions of x
            std::vector<Vec3> &xp = vp;

            int nStep = (int)(elapsed_time/time_step);

            for(int k= 0; k < nStep; k++)
            {
                #pragma omp parallel for
                for(int i = 0; i<mesh.nV; i++)
                    if (!isConstrainedAtVertex(i))
                    {
                        // Step 1
                        vel[i] = vel[i] + 0.5*time_step*((forces[i] - material.damping*vel[i])/mass[i]);
                        // Step 2
                        xp[i] = x[i];
                        x[i] += vel[i]*time_step;
                    }
                // Step 3
                computeForces();

                #pragma omp parallel for
                for(int i = 0; i<mesh.nV; i++)
                    if (!isConstrainedAtVertex(i))
                    {
                        // Step 4
                        vel[i] = vel[i] + 0.5*time_step*((forces[i] - material.damping*vel[i])/mass[i]);
                        // Step 5
                        x[i] = xp[i] + vel[i]*time_step;
                        // Enforcement of constraints
                        constraints.resolve(x[i],vel[i]);

                    }
            }
        }
        //! implicit Runge-Kutta integrator
        void irk2(double elapsed_time)
        {
            const static int nsd = 2, nmd = 3;
            std::vector<Vec3> F[nsd], YH, QQ, FS, PS, ZQ[nsd];
            static double C[nsd],AA[nsd][nsd],E[nsd][nsd+nmd],B[nsd],BC[nsd], SM[nmd],AM[nsd+nmd];
            double uround=1e-16;
            int nitmax = 10;
            const int &ns = nsd;
            double& h = time_step;

            //pre-computed coefficients
            if (ns == 2)
            {
                C[0]= 0.21132486540518711775;
                C[1]= 0.78867513459481288225;
                B[0]= 0.50000000000000000000;
                B[1]= 0.50000000000000000000;
                BC[0]= 0.39433756729740644113;
                BC[1]= 0.10566243270259355887;
                AA[0][0]= 0.41666666666666666667e-1;
                AA[0][1]=-0.19337567297406441127e-1;
                AA[1][0]= 0.26933756729740644113e+0;
                AA[1][1]= 0.41666666666666666667e-1;
                E[0][0]=-0.28457905077110526160e-02;
                E[0][1]=-0.63850024471784160410e-01;
                E[0][2]= 0.48526095198694517563e-02;
                E[0][3]= 0.11305688530429939012e+00;
                E[0][4]=-0.28884580475413403312e-01;
                E[1][0]= 0.41122751744511433137e-01;
                E[1][1]=-0.18654814888622834132e+00;
                E[1][2]=-0.18110185277445209332e-01;
                E[1][3]= 0.36674109449368040786e+00;
                E[1][4]= 0.10779872188955481745e+00;
                SM[0]= 0.00000000000000000000e+00;
                SM[1]= 0.10000000000000000000e+01;
                SM[2]= 0.16000000000000000000e+01;
                AM[0]= 0.25279583039343438291e+02;
                AM[1]=-0.86907830393434382912e+01;
                AM[2]=-0.80640000000000000000e+00;
                AM[3]= 0.29184000000000000000e+01;
                AM[4]= 0.00000000000000000000e+00;
            }
            double h2 = h*h;
            for (int i = 0; i<ns; i++)
            {
                    B[i] *= h;
                    BC[i] *= h2;
                    C[i] *= h;
                    for(int j = 0; j<ns; j++)
                    {
                            AA[i][j] *=h2;
                            E[i][j] *=h2;
                    }

            }
            for(int i1 = 0; i1<nmd; i1++)
            {
                    for(int i2 = 0; i2<ns; i2++)
                    {
                            E[i2][i1+ns] *=h2;
                    }
                    AM[ns+i1] *=h;
            } // end coefficients

            std::vector<Vec3> &Q = mesh.v;
            std::vector<Vec3> &P = vel;
            unsigned int &N = mesh.nV;

            FS.resize(N);
            calcAcc(FS);

            for(int is = 0; is<ns; is++)
            {
                ZQ[is].resize(N);
                F[is].resize(N);
                double FAC = C[is]*C[is]/2.0;
                for (int i = 0; i<N; i++)
                        ZQ[is][i] = C[is]*P[i] + FAC*FS[i];

            }
            PS = P;
            YH.resize(N);
            QQ.resize(N);
            
            // main loop
            
            int nStep = (int)(elapsed_time/time_step);
            std::vector<Vec3> Qp; // temporary vector
            for(int iter = 0; iter < nStep; iter++)
            {
                Qp = Q;
                // Update
                if (iter > 0)
                {
                  int ns1 = ns;
                  int ns2 = ns + 1;
                  int nsm = ns + nmd - 1;

                  #pragma omp parallel for
                  for( int i = 0; i < N; i++)
                  {
                      Vec3 sav(0.0,0.0,0.0);
                      for(int js = 0; js<ns; js++)
                            sav += AM[js]*ZQ[js][i];
                      YH[i] = sav + AM[ns1]*PS[i] + AM[ns2]*P[i]+Qp[i];
                      for(int is=0; is < ns; is++)
                      {
                          Vec3 sav(0.0,0.0,0.0);
                          for(int js = 0; js < ns; js++)
                               sav += E[is][js]*F[js][i];
                          ZQ[is][i] = sav + E[is][ns1]*FS[i];
                      }
                  }
                  calcAcc(FS);
                  Q = YH;
                  calcAcc(F[0]);

                  #pragma omp parallel for
                  for(int i = 0; i<N; i++)
                  {
                      PS[i] = P[i];
                      for (int is = 0; is < ns;is++)
                      {
                          ZQ[is][i] += E[is][ns2]*FS[i]+E[is][nsm]*F[0][i] + C[is]*P[i];
                      }
                  }

                }
                // fixed point iteration
                int niter = 0;
                double dynold = 0.0;
                double dyno = 1.0;
                while (dyno > uround)
                {
                        for(int js=0; js<ns; js++)
                        {
                                for(int j=0; j<N; j++)
                                        Q[j] = Qp[j] + ZQ[js][j];
                                calcAcc(F[js]);
                        }
                        dyno = 0.0;
                        for(int i = 0; i<N; i++)
                        {
                                double dnom = 1e-1;
                                for(int is = 0; is<ns; is++)
                                {
                                        Vec3 sum = C[is]*P[i];

                                        for(int js=0; js<ns; js++)
                                                sum += AA[is][js]*F[js][i];
                                        dyno += (sum - ZQ[is][i]).squaredNorm()/(dnom*dnom);
                                        ZQ[is][i] = sum;
                                }

                        }
                        dyno = sqrt(dyno/(ns*N));

                        niter++;
                        if ((dynold < dyno) && (dyno < 10*uround))
                                break;
                        if (niter >= nitmax)
                        {
                                //std::cout << "no convergence of iteration, error = " << dyno << std::endl;
                                //return;
                            break;
                        }
                        dynold = dyno;
                }
                // update of the solution
                #pragma omp parallel for
                for(int i = 0; i < N; i++)
                {
                    if (!isConstrainedAtVertex(i))
                    {
                        Vec3 sum(0.0,0.0,0.0);
                        for(int is = 0; is<ns; is++)
                                sum += F[is][i]*BC[is];
                        Qp[i] += h*P[i] + sum;
                    }

                    Q[i] = Qp[i];

                    if (!isConstrainedAtVertex(i))
                    {

                        Vec3 sum(0.0,0.0,0.0);
                        for(int is = 0; is<ns; is++)
                                sum += F[is][i]*B[is];
                        P[i] += sum;

                        // Enforcement of constraints
                        constraints.resolve(Q[i],P[i]);
                    }
                }
            }

        }
        
        //! Compute quantities of the original mesh
		void init()
		{
            time_step = 1e-5;

			constrainedVertices.resize(mesh.nV,0);
			computeLumpedMass();

			forces.resize(mesh.nV,Vec3(0.0,0.0,0.0));
			vel.resize(mesh.nV,Vec3(0.0,0.0,0.0));
            vp.resize(mesh.nV);
						
			//! Compute rest area, length and hinge
			rest_face_area.resize(mesh.nF);
			face_normals.resize(mesh.nF);
			face_area.resize(mesh.nF);
#pragma omp parallel for
			for(int i = 0; i < mesh.nF; i++)
			{
				face_normals[i] = mesh.getFaceAreaVec(i);
				rest_face_area[i] = 0.5*face_normals[i].norm();
				face_normals[i].normalize();
			}
			
			edge_vector.resize(3*mesh.nF, Vec3(0.0,0.0,0.0));
			edge_length.resize(3*mesh.nF, 0.0f);
			rest_edge_length.resize(3*mesh.nF, 0.0f);

			dihedral_angle.resize(3*mesh.nF, 0.0f);
			rest_dihedral_angle.resize(3*mesh.nF,0.0f);
						
			// loop through all edges
#pragma omp parallel for			
			for(int edge = 0; edge < 3*mesh.nF; edge++)
			{
				int j = mesh.initial(edge);
				int i = mesh.terminal(edge);
				edge_vector[edge] = mesh.v[j] - mesh.v[i];
				rest_edge_length[edge] = edge_vector[edge].norm();
				if (mesh.isInnerEdge(edge))
				{
					int f1 = mesh.face(edge);
					int f2 = mesh.face(mesh.opposite(edge));
					rest_dihedral_angle[edge] = atan2(face_normals[f2].cross(face_normals[f1]).dot(edge_vector[edge]), rest_edge_length[edge]*face_normals[f2].dot(face_normals[f1]));
				}
			}
#if IMPLICIT
			vring.resize(mesh.nV);
            
#pragma omp parallel for
            for(int i = 0; i < mesh.nV; i++)
				mesh.oneringIdx(i, vring[i]);
			std::vector<int> valIdx(mesh.nV);
			int numVal = 0;
			for(int i = 0; i < mesh.nV; i++)
			{
				valIdx[i] = numVal;
				numVal += vring[i].size();
			}
			int numEntries = 3*3*numVal;
            val.resize(numEntries);
			rowIdx.resize(numEntries);
			colPtr.resize(3*mesh.nV + 1);
			
            fHess.resize(mesh.nF);
            fForces.resize(mesh.nF);
#pragma omp parallel for
			for(int i = 0; i < mesh.nV; i++)
            {
                forces[i] << 0.0,0.0,0.0;
				for (int j = 0; j < 3; j++)
					colPtr[3*i + j] = 9*valIdx[i] + 3*j*vring[i].size();
			}
			colPtr[3*mesh.nV] = numEntries;
#pragma omp parallel for
            for(int i = 0; i < mesh.nV; i++)
            {
				for(int j = 0; j < vring[i].size(); j++)
				{
                	for(int i1 = 0; i1 < 3; i1++)
						for(int i2 = 0; i2 < 3; i2++)
							rowIdx[colPtr[3*i + i1] + 3*j + i2] = 3*vring[i][j].first + i2;
				}	
            }  
#endif
		}
        
        void calcAcc(std::vector<Vec3>& Acc)
        {

           computeForces();

           for(int i = 0; i < mesh.nV; i++)
		   {
			   Acc[i] = Vec3(0.0,-9.8,0.0) + (forces[i] - material.damping*vel[i])/mass[i];
               //Acc[i] = forces[i]/mass[i];
		   }

        }
#if IMPLICIT
        void assembleImpMatrix()
        {
            //calculate area and normals for each face
#pragma omp parallel for
            for(int i = 0; i < mesh.nF; i++)
            {
                face_normals[i] = mesh.getFaceAreaVec(i);
                face_area[i] = 0.5*face_normals[i].norm();
                face_normals[i].normalize();
            }
            // calculate edge vector, edge lenth and dihedral angle for each edge
#pragma omp parallel for
            for(int edge = 0; edge < 3*mesh.nF; edge++)
            {
                int j = mesh.initial(edge);
                int i = mesh.terminal(edge);
                edge_vector[edge] = mesh.v[j] - mesh.v[i];
                edge_length[edge] = edge_vector[edge].norm();
                edge_vector[edge] /= edge_length[edge]; // normalized
                if (mesh.isInnerEdge(edge) && j < i)
                {
                    int f1 = mesh.face(edge);
                    int f2 = mesh.face(mesh.opposite(edge));
                    dihedral_angle[edge] = atan2(face_normals[f2].cross(face_normals[f1]).dot(edge_vector[edge]), face_normals[f2].dot(face_normals[f1]));
                }
            }
            //loop over each face and calculate force and Hessian
#pragma omp parallel for
            for (int j = 0; j < mesh.nF; j++)
            {
                double cos_beta[3], h[3], c[3], d[3];
                Eigen::Matrix3d M[3];
                Vec3 &n = face_normals[j];
				Vec3 m[3];
				for(int i = 0; i < 3; i++)
					m[i] = edge_vector[i].cross(n);
                for(int i = 0; i < 3; i++)
                {
                    int eidx = 3*j + (i+1)%3; // opposite edge to the vertex i
                    cos_beta[i] = -edge_vector[3*j + i].dot(edge_vector[3*j + (i+2)%3]);
                    h[i] = 2*face_area[j]/edge_length[eidx];
					M[i] = n*(n.cross(edge_vector[i]).transpose());
                    if (mesh.isInnerEdge(eidx))
                    {
                        int opp_eidx = mesh.opposite(eidx);
                        double delta_theta = (mesh.f[j][(i+1)%3] < mesh.f[j][(i+2)%3])?(dihedral_angle[eidx] - rest_dihedral_angle[eidx]):(dihedral_angle[opp_eidx]-rest_dihedral_angle[eidx]);
                        c[i] = 6*material.kTheta*delta_theta*edge_length[eidx]/(rest_face_area[j] + rest_face_area[mesh.face(opp_eidx)]);
                    }
                    else
                        c[i] = 0.0;

                }
				
                for (int i = 0; i < 3; i++)
                    d[i] = c[(i+2)%3]*cos_beta[(i+1)%3] + c[(i+1)%3]*cos_beta[(i+2)%3] - c[i];

				fHess[j].setZero();
				fForces[j].setZero();
                // Hessian for bending energy

                for (int i = 0; i < 3; i++)
                {
                   fHess[j].block<3,3>(3*i,3*i) = (1/(h[i]*h[i]))*d[i]*(M[i].transpose() + M[i]) - (c[(i+1)%3]/(edge_length[3*j + (i+2)%3]*edge_length[3*j + (i+2)%3]))*M[(i+1)%3] -
                           - (c[(i+2)%3]/(edge_length[3*j + i]*edge_length[3*j + i]))*M[(i+2)%3] ;
                   int jj = (i+1)%3;
                   fHess[j].block<3,3>(3*i,3*jj) = (1/(h[i]*h[jj]))*(d[jj]*M[i].transpose() + d[i]*M[jj]) + (c[(i+2)%3]/(edge_length[3*j + i]*edge_length[3*j + i]))*M[(i+2)%3] ;
                }

				fHess[j].block<3,3>(3*1,3*0) = fHess[j].block<3,3>(3*0,3*1).transpose();
                fHess[j].block<3,3>(3*2,3*1) = fHess[j].block<3,3>(3*1,3*2).transpose();
                fHess[j].block<3,3>(3*0,3*2) = fHess[j].block<3,3>(3*2,3*0).transpose();

    

                for(int i = 0; i < 3; i++) // loop over edges
                {
                    int eidx = 3*j + i;
                    if (mesh.isInnerEdge(eidx))
                    {
                        int opp_eidx = mesh.opposite(eidx);
						Vec3 bf[3];
                        bf[i] = -(cos_beta[(i+1)%3]*edge_length[3*j + (i+1)%3])*n; // bending forces
                        bf[(i+1)%3] = -(cos_beta[i]*edge_length[3*j + (i+2)%3])*n;
                        bf[(i+2)%3] = edge_length[eidx]*n;
                        double coeff = 6*material.kTheta*edge_length[eidx]/(rest_face_area[j] + rest_face_area[mesh.face(opp_eidx)]);
                        for(int ii = 0; ii < 3; ii++)
                        {
                            for(int jj = 0; jj < 3; jj++)
                                 fHess[j].block<3,3>(3*ii,3*jj) += coeff*bf[ii]*bf[jj].transpose();
                            double delta_theta = (mesh.f[j][ii] < mesh.f[j][(ii+1)%3])?(dihedral_angle[eidx] - rest_dihedral_angle[eidx]):(dihedral_angle[opp_eidx]-rest_dihedral_angle[eidx]);
                            fForces[j].col(ii) += coeff*delta_theta*bf[ii];
                        }

                        fForces[j].col(i) -= (2*material.kE*(edge_length[eidx]/rest_edge_length[eidx]-1))*edge_vector[eidx]; // edge (spring) force

                        //Hessian for edge force
                        Eigen::Matrix3d tmat = edge_vector[eidx]*edge_vector[eidx].transpose();
						Eigen::Matrix3d hmat = (2*material.kE/rest_edge_length[eidx])*(-tmat + (rest_edge_length[eidx]/edge_length[eidx]-1)*(Eigen::Matrix3d::Identity(3,3) - tmat));
                        fHess[j].block<3,3>(3*i,3*i) += hmat;
                        fHess[j].block<3,3>(3*i,3*((i+1)%3)) -= 0.5*hmat;
                    }
                    else
                    {
                        Vec3 v = (2*material.kE*(edge_length[eidx]/rest_edge_length[eidx]-1))*edge_vector[eidx];
                        fForces[j].col(i) -= v;
                        fForces[j].col((i+1)%3) += v;
                        //Hessian for edge force
                        Eigen::Matrix3d tmat = edge_vector[eidx]*edge_vector[eidx].transpose();
                        Eigen::Matrix3d hmat = (2*material.kE/rest_edge_length[eidx])*(-tmat + (rest_edge_length[eidx]/edge_length[eidx]-1)*(Eigen::Matrix3d::Identity(3,3) - tmat));
																	
                        fHess[j].block<3,3>(3*i,3*i) += hmat;
                        fHess[j].block<3,3>(3*i,3*((i+1)%3)) -= hmat;
                        fHess[j].block<3,3>(3*((i+1)%3),3*i) -= hmat;
                        fHess[j].block<3,3>(3*((i+1)%3),3*((i+1)%3)) += hmat;
                    }
                }
				
                // Hessian for area energy
                for (int i = 0; i < 3; i++)
                {
					fHess[j].block<3,3>(3*i,3*i) -= (material.kA/rest_face_area[j])*(0.5*edge_length[3*j+(i+1)%3]*edge_length[3*j+(i+1)%3]*m[(i+1)%3]*m[(i+1)%3].transpose() + (rest_face_area[j]- face_area[j])*(edge_length[3*j+(i+1)%3]/h[i])*toSkewSymMat(edge_vector[3*j+(i+1)%3])*M[i].transpose());
                    // Area force
					fForces[j].col(i) += material.kA*(1 - face_area[j]/rest_face_area[j])*edge_length[3*j + (i+1)%3]*m[(i+1)%3];
                }
				fHess[j].block<3,3>(3*0,3*1) -= (material.kA/rest_face_area[j])*(0.5*edge_length[3*j+1]*edge_length[3*j+2]*m[1]*m[2].transpose() + (rest_face_area[j]- face_area[j])*((edge_length[3*j+1]/h[1])*toSkewSymMat(edge_vector[3*j+1])*M[1].transpose()-toSkewSymMat(n)));
				fHess[j].block<3,3>(3*0,3*2) -= (material.kA/rest_face_area[j])*(0.5*edge_length[3*j+1]*edge_length[3*j]*m[1]*m[0].transpose() + (rest_face_area[j]- face_area[j])*((edge_length[3*j+1]/h[2])*toSkewSymMat(edge_vector[3*j+1])*M[2].transpose()+toSkewSymMat(n)));
				fHess[j].block<3,3>(3*1,3*2) -= (material.kA/rest_face_area[j])*(0.5*edge_length[3*j+2]*edge_length[3*j]*m[2]*m[0].transpose() + (rest_face_area[j]- face_area[j])*((edge_length[3*j+2]/h[2])*toSkewSymMat(edge_vector[3*j+2])*M[2].transpose()-toSkewSymMat(n)));

				fHess[j].block<3,3>(3*1,3*0) = fHess[j].block<3,3>(3*0,3*1).transpose();
                fHess[j].block<3,3>(3*2,3*1) = fHess[j].block<3,3>(3*1,3*2).transpose();
                fHess[j].block<3,3>(3*2,3*0) = fHess[j].block<3,3>(3*0,3*2).transpose();

            }
            // sum the forces and Hessian up
			
#pragma omp parallel for
			for(int i = 0; i < val.size(); i++)
				val[i] = 0.0;

//#pragma omp parallel for
            for(int i = 0; i < mesh.nV; i++)
            {
                forces[i] << 0.0,0.0,0.0;
                int first_edge = mesh.edge(i);
                int edge = first_edge;
                for (int iter = 1;;iter++)
                {
                    int face = mesh.face(edge); // the face associated with this edge
                    int j_idx = mesh.local(edge);
                    int i_idx = (j_idx+1)%3;
                    int k_idx = (i_idx+1)%3;
                    int j = mesh.initial(edge);
                    int k = mesh.op_vertex(edge);

                    forces[i] += fForces[face].col(i_idx);

                    for(int i1 = 0; i1 < 3; i1++)
                        for(int i2 = 0; i2 < 3; i2++)
                        {
							val[colPtr[3*i + i1] + 3*vring[i][0].second + i2] += fHess[face](3*i_idx + i2, 3*i_idx + i1);
                            val[colPtr[3*i + i1] + 3*vring[i][iter].second + i2] += fHess[face](3*j_idx + i2, 3*i_idx + i1);
                            val[colPtr[3*i + i1] + 3*vring[i][iter].second + 3 + i2] += fHess[face](3*k_idx + i2, 3*i_idx + i1);
                        }

                    edge = mesh.next(edge);
                    if (mesh.isBoundaryEdge(edge) ) break;
                    edge = mesh.opposite(edge);
                    if (edge == first_edge) break;

                }
            }
			
			
        } // end assembleImplMatrix
		
		//! Newmark's beta method
        void newmark(double elapsed_time, double beta, double lambda)
        {
			// x is the position of our nodes
            std::vector<Vec3> &x = mesh.v;
            // xp hold the previous positions of x
            std::vector<Vec3> &xp = vp;

			double &h = time_step;
			double h2 = h*h;

			std::vector<double> b(3*mesh.nV);
			std::vector<double> xs(3*mesh.nV);
			
			int max_iter = 5;
			
			int nStep = (int)(elapsed_time/time_step);

            for(int k= 0; k < nStep; k++)
            {
				assembleImpMatrix();

				std::vector<Vec3> tmpF = forces;
	#pragma omp parallel for
				for(int i = 0; i<mesh.nV; i++)
					xp[i] = x[i] + h*vel[i] + (0.5-beta)*h2*forces[i]/mass[i]; 
			
				// Newton-Raphson loop
				double err = 0;
				for(int iter = 0; iter < max_iter; iter++)
				{
					if (iter > 0) assembleImpMatrix();
	#pragma omp parallel for
					for(int i = 0; i < val.size(); i++)
						val[i] *= beta*h2;
	#pragma omp parallel for
					for (int i = 0; i < mesh.nV; i++)
						for(int j = 0; j < 3; j++)
							val[colPtr[3*i + j] + 3*vring[i][0].second + j] = 1 - val[colPtr[3*i + j] + 3*vring[i][0].second + j]/mass[i];
					
	#pragma omp parallel for
					for (int i = 0; i < mesh.nV; i++)
						for(int j = 0; j < 3; j++)
							b[3*i + j] = x[i][j] - xp[i][j] -  beta*h2*forces[i][j]/mass[i];
					
                    pardiso_solve(3*mesh.nV,colPtr.data(),rowIdx.data(),val.data(),b.data(),xs.data());
				
	#pragma omp parallel for
					for(int i = 0; i < mesh.nV; i++)
						if (!isConstrainedAtVertex(i))
							for(int j = 0; j < 3; j++)
								x[i][j] -= xs[3*i + j];
					double tmp = err; err = 0;
					std::for_each(xs.begin(),xs.end(),[&err](double x){err += x*x;});
					err = sqrt(err);
					if (fabs(err - tmp) < 1e-6 || err < 1e-6) break;		
				}
#pragma omp parallel for
			for(int i = 0; i < mesh.nV; i++)
				if (!isConstrainedAtVertex(i))
				{
					vel[i] += (h/mass[i])*((1-lambda)*tmpF[i] + lambda*forces[i]);
					// Enforcement of constraints
                    constraints.resolve(x[i],vel[i]);
				}
			}
        }
        Eigen::Matrix3d toSkewSymMat(Vec3& v)
        {
            Eigen::Matrix3d mat;
            mat << 0, -v[2], v[1],
                   v[2], 0, -v[0],
                   -v[1], v[0], 0;
            return mat;
        }
#endif


	};



