struct DiscreteShellMaterial
	{
		double kE;
		double kA;
		double kTheta;
		double mass_density;
		double damping;
		DiscreteShellMaterial():kE(1e6), kA(1e6), kTheta(1e1), mass_density(1e3), damping(1e3){}
};
struct VolumeMaterial
{
    double poisson_ratio;
    double young_modulus;
    double mass_density;
    double damping;
    double Lambda;
    double Mu;
    VolumeMaterial():young_modulus(1e6),poisson_ratio(0.35),mass_density(1e3),damping(1e1)
    {
        Lambda = young_modulus*poisson_ratio/((1 + poisson_ratio)*(1 - 2*poisson_ratio));
        Mu = young_modulus/(2*(1 + poisson_ratio));
    }
};
