#ifndef COLLISIONDETECTION_H
#define COLLISIONDETECTION_H

#include <unordered_map>
#include <Eigen/Core>

typedef Eigen::Vector3d Vec3d;

#define remainder(x,n) (x >= 0)?(x)%(n):((n) + (x)%(n))%(n)

struct Cell
{
	int i,j,k;
	
	Cell(Vec3d const &v, double gridSize)
	{
		i = (int)floor(v[0]/gridSize);
		j = (int)floor(v[1]/gridSize);
		k = (int)floor(v[2]/gridSize);
	}
	Cell(int idx, int idy, int idz):i(idx),j(idy),k(idz){}

	Cell operator+(Cell const &c)
	{
		return Cell(i + c.i,j + c.j,k + c.k);
	}
	
};
struct Triangle
{
	int index[3];
	int time_stamp;
	Triangle(int idx1, int idx2, int idx3)
	{
		index[0] = idx1;
		index[1] = idx2;
		index[2] = idx3;
		time_stamp = 0;
	}
};
struct iequal_to:std::binary_function<Cell,Cell,bool>
{
	bool operator()(Cell const &c1, Cell const & c2 ) const
	{
		return (c1.i == c2.i && c1.j == c2.j && c1.k == c2.k);
	}
};

struct ihash:std::unary_function<Cell,std::size_t>
{
	int hash_size;
	ihash(){}
	ihash(int n):hash_size(n){}
	std::size_t operator()(Cell const &c) const 
	{
		int p = (c.i * 73856093) ^  (c.j * 19349663) ^ (c.k * 83492791);
		return remainder(p, hash_size);
	}
};
typedef std::unordered_multimap<Cell,Vec3d,ihash,iequal_to> multi_map;
typedef std::unordered_multimap<Cell,Triangle*,ihash,iequal_to> multi_map_tri;

template<class T>
void boundingBox(T v1, T v2, T v3, T v4, T& lower, T& upper)
{
        for (int i = 0; i < 3; i++)
        {
                lower[i] = min(min(min(v1[i],v2[i]),v3[i]),v4[i]);
                upper[i] = max(max(max(v1[i],v2[i]),v3[i]),v4[i]);
        }
}
template<class T>
void boundingBox(T v1, T v2, T v3, T& lower, T& upper)
{
        for (int i = 0; i < 3; i++)
        {
                lower[i] = min(min(v1[i],v2[i]),v3[i]);
                upper[i] = max(max(v1[i],v2[i]),v3[i]);
        }
}
template<class T>
bool testPointTetrahedral(T v0, T v1, T v2, T v3, T p)
{
        T V10 = v1 - v0;
        T P = p - v0;
        T Q = cross(v2 - v0,v3 - v0);
        T R = cross(V10,P);
        double det = dot(V10,Q);
        double beta1 = dot(P,Q)/det;
        double beta2 = dot(R,v3-v0)/det;
        double beta3 = -dot(R,v2-v0)/det;
        double sum = beta1 + beta2 + beta3;
        if (-1e-6<= beta1 && beta1 <= 1+1e-6 && -1e-6<= beta2 && beta2 <=
			1+1e-6 && -1e-6<= beta3 && beta3 <= 1+1e-6  && -1e-6<= sum && sum <=
			1+1e-6) 
			return true;
        else return false;

}
template<class T>
bool testRayTriangle(T origin, T direction, T v0, T v1, T v2, float &t, T &n)
{
	T v10 = v1 - v0;
	T v20 = v2 - v0;
	n = cross(v10,v20);
	float d = -dot(direction,n);
	if (d <= 0.0) return 0;
	T ov0 = origin - v0;
	t = dot(ov0,n);
	if (t < 0.0) return 0;
	T e = cross(direction, ov0);
	float v = -dot(v20,e);
	if (v < 0.0 || v > d) return 0;
	float w = dot(v10,e);
	if (w < 0.0 || v + w > d) return 0;
	t /= d;
	/*v /= d;
	w /= d;*/
	return 1;
}

template<class T>
bool testSegmentTriangle(T origin, T dest, T v0, T v1, T v2, float& v, float& w)
{
	T v10 = v1 - v0;
	T v20 = v2 - v0;
	T n = cross(v10,v20);
	
	T direction = dest - origin;
	float d = -dot(direction,n);
	
	if (d <= 0.0) return 0;
	T ov0 = origin - v0;
	float t = dot(ov0,n);
	
	if (t < 0.0) return 0;
	T e = cross(direction, ov0);
	v = -dot(v20,e);
	if (v < 0.0 || v > d) return 0;
	w = dot(v10,e);
	if (w < 0.0 || v + w > d) return 0;
	t /= d;
	v /= d;
	w /= d;
		
	if ( t<=1.0 && v <= 1.0 && w <= 1.0 && v + w <= 1.0) return 1; else return 0;
}

void solidCollisionTest(int numPoints1, float* points1, int numPoints2, float* points2, int numElems, int* elem2, float gridSize);
void reportSolidCollision(Vec3d v0,Vec3d v1,Vec3d v2, Vec3d v3, multi_map::const_iterator it);

void rayTracingCollisionDetection(int numVertices1, float* vertices1, float* normals1, int numVertices2, float* vertices2, int numElems2, int* elems2);
bool segmentCollisionDetection(float* startPt, float* endPt, int numVertices2, float* vertices2, int numElems2, int* elems2,
								  int& idx1, int& idx2, int& idx3, float& v, float& w);

#endif
