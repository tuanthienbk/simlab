class TriangularMesh
	{
		
	public:
		/// number of vertices, faces and edges
		unsigned int nV, nF, nE;
		std::vector<Eigen::Vector3d> v; // vertex list
		std::vector<Eigen::Vector3d> normals; // normal list
		std::vector<Eigen::Vector3i> f; // face list
	private:
		std::vector<int> twin; 
		std::vector<int> edge_map;
	public:
		void computeNormals()
		{
			normals.resize(nV,Eigen::Vector3d(0.0f, 0.0f, 0.0f));
			
			for(int i = 0; i < nF; i++)
				for(int j = 0; j < 3; j++)
					normals[f[i][j]] += getFaceAreaVec(i);
			for(int i = 0; i < nV; i++)
				normals[i].normalize();
		}
		double getFaceArea(int fi)
		{
			return  0.5*(v[f[fi][1]] - v[f[fi][0]]).cross(v[f[fi][2]] - v[f[fi][0]]).norm();
			
		}
		Eigen::Vector3d getFaceAreaVec(int fi)
		{
			return (v[f[fi][1]] - v[f[fi][0]]).cross(v[f[fi][2]] - v[f[fi][0]]);
		}
		void readOff(const char* filename)
		{
			FILE *offfile = fopen(filename,"r");
			if (offfile == NULL)
			{
				throw std::exception("File not found!");
				return;
			}
			char header[5];
			fscanf(offfile,"%s\n", header);
			if (strcmp(header,"OFF"))
			{
				throw std::exception("This is not OFF file");
				return;
			}
			fscanf(offfile,"%d %d%*[^\n]", &nV, &nF);
			v.resize(nV);
			f.resize(nF);
			
			Eigen::SparseMatrix<int> edges(nV,nV); 
			float x, y, z;
			for(int i = 0; i < nV; i++)
			{
				fscanf(offfile,"%f %f %f%*[^\n]",&x, &y, &z);
                v[i] << double(x), double(y), double(z);
               
			}
			
			std::vector<Eigen::Triplet<int>> tripletList;
			nE = nV + nF - 1; // Euler's formula
			tripletList.reserve(3*nF);
			for(int i = 0; i < nF; i++)
			{
				int np;
				fscanf(offfile,"%d %d %d %d%*[^\n]",&np, &f[i][0], &f[i][1], &f[i][2]);
				tripletList.push_back(Eigen::Triplet<int>(f[i][0],f[i][1],3*i+1));
				tripletList.push_back(Eigen::Triplet<int>(f[i][1],f[i][2],3*i+2));
				tripletList.push_back(Eigen::Triplet<int>(f[i][2],f[i][0],3*i+3));	
			}
			fclose(offfile);
			edges.setFromTriplets(tripletList.begin(), tripletList.end());
			buildHalfEdge(edges);
			computeNormals();
		}
		void ex2off(const char* filename)
		{
			FILE *offfile = fopen(filename,"w");
			fprintf(offfile,"OFF\n");
			fprintf(offfile,"%d %d\n", nV, nF);
			for(int i = 0; i < nV; i++)
				fprintf(offfile,"%f %f %f% \n",v[i][0],v[i][1],v[i][2]);
			
			for(int i = 0; i < nF; i++)
				fprintf(offfile,"%d %d %d %d \n",3, f[i][0],f[i][1],f[i][2]);
			fclose(offfile);
		}
		void ex2poly(const char* filename)
		{
			FILE *file = fopen(filename,"w");

			fprintf(file,"# part 1 - node list \n# <num_nodes> <dimension> <attributes> <marks> \n");
			fprintf(file,"%d %d %d %d\n", nV, 3, 0, 0);
			for(int i = 0; i < nV; i++)
				fprintf(file,"%d %f %f %f% \n",i,v[i][0],v[i][1],v[i][2]);

			fprintf(file,"# part 2 - face list \n# <num_faces> <marks> \n");
			fprintf(file,"%d %d\n", nF, 0);
			for(int i = 0; i < nF; i++)
				fprintf(file,"%d\n%d %d %d %d \n",1,3,f[i][0],f[i][1],f[i][2]);

			fprintf(file,"# part 3 - hole list \n");
			fprintf(file,"%d \n", 0);
			
			fprintf(file,"# part 4 - region list \n");
			fprintf(file,"%d \n", 0);

			fclose(file);
		}
		void readObj(char* filename);

		//! create mesh structure from raw data
		void makeMesh(double* vertices, int nv, int* face, int nf)
		{
			nV = nv;
			nF = nf;
			v.resize(nv);
			f.resize(nf);

			Eigen::SparseMatrix<int> edges(nV,nV);  // edge graph
			
			for(int i = 0; i < nv; i++)
				v[i]<<vertices[3*i], vertices[3*i+1], vertices[3*i+2];
			
			std::vector<Eigen::Triplet<int>> tripletList;
			nE = nV + nF - 1; // Euler's formula
			tripletList.reserve(3*nF);
			for(int i = 0; i < nf; i++)
			{
				f[i] << face[3*i], face[3*i+1], face[3*i+2];
				tripletList.push_back(Eigen::Triplet<int>(f[i][0],f[i][1],3*i+1));
				tripletList.push_back(Eigen::Triplet<int>(f[i][1],f[i][2],3*i+2));
				tripletList.push_back(Eigen::Triplet<int>(f[i][2],f[i][0],3*i+3));
			}
			edges.setFromTriplets(tripletList.begin(), tripletList.end());
			buildHalfEdge(edges);
			computeNormals();
		}
        //! Compute valence
        int valence(int vi)
        {
            int val = 0;
            int first_edge = edge(vi);
            int next_edge = first_edge;
            while (1)
            {
				val++;
                next_edge = next(next_edge);
				if (isBoundaryEdge(next_edge)) {
					val++;		
					break;
				}
				next_edge = opposite(next_edge);
				if (next_edge == first_edge) break;
            }
            return val;
        }
		//! Find 1-ring of the vertex with index vi
		void oneringv(int vi, std::vector<int> &orv)
		{
			int first_edge = edge(vi);
			int next_edge = first_edge;
            while (1)
            {
				orv.push_back(initial(next_edge));
				next_edge = next(next_edge);
				if (isBoundaryEdge(next_edge)) {
					orv.push_back(terminal(next_edge));		
					break;
				}
				next_edge = opposite(next_edge);
				if (next_edge == first_edge) break;
            }
			
		}
		/** Find 1-ring sorted indices around a vertex vi
		*   Example: vi = 0, the neighbor vertices are 3,4,1 clock-wise, we have array 0,3,4,1.
		*	After sorted we have 0,1,3,4. We want to map the original index to the sorted index. 
		*	For instance, the original index of 3 is 1, the sorted index of 3 is 2, so we map 1 -> 2. 
		*	Eventually, the output is (the mapping is from index -> element.second)
		*	0 -> (0, 0)
		*	1 -> (1, 2)
		*	2 -> (3, 3)
		*	3 -> (4, 1)
		*/
		void oneringIdx(int vi, std::vector<std::pair<int,int> >& idx)
		{
			std::vector<std::pair<int,int> > orv;
			int inc = 0;
			orv.push_back(std::pair<int,int>(vi,inc));
			int first_edge = edge(vi);
			int next_edge = first_edge;
            while (1)
            {
				orv.push_back(std::pair<int,int>(initial(next_edge),++inc));
				next_edge = next(next_edge);
				if (isBoundaryEdge(next_edge)) {
					orv.push_back(std::pair<int,int>(terminal(next_edge),++inc));		
					break;
				}
				next_edge = opposite(next_edge);
				if (next_edge == first_edge) break;
            }
			
			std::sort(orv.begin(), orv.end(), [&orv](std::pair<int,int> l, std::pair<int,int> r){return l.first < r.first;});
			idx.resize(orv.size());
			for(int i = 0; i < orv.size(); i++)
				idx[orv[i].second] = std::pair<int,int>(orv[orv[i].second].first,i);
		}
		//! Find 1-ring face of the vertex
		void oneringf(int vi, std::vector<int> &orf)
		{
			int first_edge = edge(vi);
			int next_edge = first_edge;
            while (1)
            {
				orf.push_back(face(next_edge));
				next_edge = next(next_edge);
				if (isBoundaryEdge(next_edge)) break;
				next_edge = opposite(next_edge);
				if (next_edge == first_edge) break;
            }
		}

		//! incident face of the edge
		int face(int edge) { return edge/3;}
		//! initial vertex of the edge
		int initial(int edge) { return f[face(edge)][edge%3];}
		//! terminal vertex of the edge
		int terminal(int edge) { return f[face(edge)][(edge + 1)%3];}
		//! opposite vertex in the triangle
		int op_vertex(int edge) { return f[face(edge)][(edge + 2)%3];}
		//! local index of the edge in the triangle
		int local(int edge) { return edge%3;}
		//! next edge in the triangle
		int next(int edge) { return 3*(edge/3) + (edge + 1)%3;}
		//! previous edge in the triangle
		int prev(int edge) { return 3*(edge/3) + (edge + 2)%3;}
		//! opposite edge (twin edge)
		int opposite(int edge) {return twin[edge];}
		//! get edge index from vertex index 
		int edge(int vertex) { return edge_map[vertex];}
		
		//! test the edge if it is on boundary
		bool isBoundaryEdge(int edge) {return twin[edge] < 0;}
		//! test the edge if it is an inner edge
		bool isInnerEdge(int edge) {return twin[edge] >= 0;}
		private:
			 
			void buildHalfEdge(Eigen::SparseMatrix<int> &edges)
			{
				twin.resize(3*nF);
				edge_map.resize(nV);
				for (int i=0; i < nV; ++i)
				{
					Eigen::SparseMatrix<int>::InnerIterator it(edges,i);
					edge_map[i] = it.value() - 1;
					for(;it;it++)
					{
						int ie = edges.coeff(it.col(),it.row())-1;
						twin[it.value()-1] = ie;
						if (ie < 0) edge_map[i] = it.value() - 1;
						
					}
					
				}		
					
			}
		
	};
