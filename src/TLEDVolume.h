#ifndef TLEDVOLUME_HPP
#define TLEDVOLUME_HPP

#include <Eigen/Core>
#include <Eigen/LU>
#include "Material.h"
#include <vector>
#include <map>


typedef Eigen::Vector3d Vec3;

class TLEDVolume
{
public:
    TLEDVolume(){}
    virtual ~TLEDVolume(){}

    void createVolume(const char* filename)
	{
		load(filename);
		init();
	}

    void doTimeStep(double elapsed_time);

	void export2off(const char* filename);

    inline void setFixedVertex(int vid) {	constrainedVertices[vid] = 1; }
    inline bool isConstrainedAtVertex( int vid ) { return constrainedVertices[vid]; }
    inline void setUnconstrainedVertex(int vid) { constrainedVertices[vid] = 0; }
    inline void setPos(int id, double* pos_) { x[id] = Eigen::Map<Vec3>(pos_,3); }
    inline Vec3 getPos(int id){return x[id];}
    inline void setVel(int id, double* vel_) { vel[id] = Eigen::Map<Vec3>(vel_,3); }
    inline void setPos(int id, Vec3 pos_) { x[id] = pos_; }
    inline void setVel(int id, Vec3 vel_) { vel[id] = vel_; }
    inline void setTimeStep(double time_step_) {time_step = time_step_;}
    inline double getTimeStep(){return time_step;}
    inline int getNbVertices(){return x0.size();}
    inline double* getPosPtr(){return (double*)x.data();}
    inline double* getNormalPtr(){return (double*)surfNormals.data();}
    inline int getNbSurfEle(){return surf.size();}
    inline int* getSurfElePtr(){return (int*)surf.data();}
    inline Eigen::Vector3i getSurfEle(int id){return surf[id];}
public:
    SurfLab::Material::VolumeMaterial material;
private:
    std::vector<bool> constrainedVertices;
    std::vector<Vec3> x0; // rest position
    std::vector<Vec3> x_prev; // previous position
    std::vector<Vec3> x; // current position
    std::vector<Vec3> vel; // current velocity
    std::vector<Eigen::Vector4i> tet; // tetrahedral
    std::vector<Vec3> forces;
    std::vector<double> mass;
    double time_step;
    std::vector<Eigen::Vector3i> surf;
    std::vector<Vec3> surfNormals;
    std::vector<Eigen::Matrix<double, 3, 4, Eigen::RowMajor> > Dh;
    std::vector<double> Volume;
    std::vector<Eigen::Matrix<double, 4, 3, Eigen::RowMajor> > elementForces; // temporary forces at vertices of a tetrahedron
    std::vector<Eigen::Vector2i> FCrds;
    int nbElementPerVertex; // maximum number of element per vertex, used for gathering forces
private:
	void init();

    void load(const char* filename);
    void generateSurfaceMesh();
    void computeSurfaceNormals();

    void computeForces();
    
    double CompElVolTetra(int i);
    /// Computes shape function global derivatives for tetrahedral elements
    void ComputeDhDxTetra(int i, double DhDr[4][3], double DhDx[4][3]);
};

#endif // TLEDVOLUME_HPP
