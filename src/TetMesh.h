#pragma once
#include <Eigen/Dense>
#include <Eigen/SparseCore>
#include "Tmesh.h"
class TetMesh
{
public:
    /// number of vertices, faces and edges
    unsigned int nV, nF, nE;
    std::vector<Eigen::Vector3d> vert; // vertex list
    std::vector<Eigen::Vector4i> tet;
    TriangularMesh surf;
    void load(const char* filename)
    {
        char ele_fname[50];
        char node_fname[50];
        sprintf(ele_fname,"%s.1.ele",filename);
        sprintf(node_fname,"%s.1.node",filename);
        FILE* f = fopen(node_fname,"r");
        if (f == NULL)
        {
            throw std::exception("File not found!");
            return;
        }
        int nV, dump;
        fscanf(f,"%d %d%*[^\n]", &nV, &dump);
        vert.resize(nV);
        float x, y, z;
    
        for(int i = 0; i < nV; i++)
        {
            fscanf(f,"%d %f %f %f%*[^\n]", &dump, &x, &y, &z);
            vert[i] << double(x), double(y), double(z);
        }
        fclose(f);
    
        f = fopen(ele_fname,"r");
        if (f == NULL)
        {
            throw std::exception("File not found!");
            return;
        }
        int nE;
        fscanf(f,"%d %d%*[^\n]", &nE, &dump);
        tet.resize(nE);
        int tet1, tet2, tet3, tet4;
        for(int i = 0; i < nE; i++)
        {
           fscanf(f,"%d %d %d %d %d%*[^\n]", &dump, &tet1, &tet2, &tet3, &tet4);
           tet[i] << tet1, tet2, tet3, tet4;
        }
    }
    void generateSurf()
    {
        for (int i=0; i<tet.size(); i++)
        {
            Eigen::Vector4i& e = tet[i];
            Eigen::Vector3i f1,f2,f3,f4;
            f1 << e[0],e[1],e[2];
            f2 << e[1],e[2],e[3];
            f3 << e[2],e[3],e[0];
            f4 << e[3],e[0],e[1];
    
            visitFace(surf,f1);
            visitFace(surf,f2);
            visitFace(surf,f3);
            visitFace(surf,f4);
    
        }
    }
private:
    bool isEquiv(const Eigen::Vector3i& f1, const Eigen::Vector3i& f2)
    {
        for(int j = 0;j<3;j++)
        {
            bool equality = false;
            for(int i = 0;i<3;i++)
                if (f2[i] == f1[j]) {equality = true;break;}
            if (!equality) return false;
        }
        return true;
    }
    void visitFace(std::vector<Eigen::Vector3i>& faceList,Eigen::Vector3i face)
    {
        for(auto it = faceList.begin();it != faceList.end();it++)
        if (isEquiv(*it,face) )
        {
            faceList.erase(it);
            return;
        }
        faceList.push_back(face);
    }

};

